let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js/app.js')
  .js('resources/assets/js/admin.js', 'public/js')
  .extract([
    'jquery',
    'bootstrap-sass',
    'vue',
    'moment',
    'eonasdan-bootstrap-datetimepicker',
    'sweetalert2'
  ])
  .autoload({
    jquery: ['$', 'window.jQuery', 'jQuery', 'jquery'],
    moment: 'moment'
  })
  .sass('resources/assets/sass/vendor.scss', 'public/css')
  .sass('resources/assets/sass/admin.scss', 'public/css')
  .sass('resources/assets/sass/app.scss', 'public/css')

if (mix.inProduction()) {
  mix.version()
} else {
  mix.sourceMaps()
}
