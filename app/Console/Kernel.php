<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Lấy post mới
        $schedule->command('v3getdailypost:list all')->twiceDaily(1, 13)->withoutOverlapping();
        // Lấy like cho post mới 
        $schedule->command('v3getreactionfornewpost:list all')->twiceDaily(7, 19)->withoutOverlapping();
        // Lấy danh sách like cho post cũ
        $schedule->command('v3getreactionforoldpost:list all')->weeklyOn(1)->withoutOverlapping();
        // Lấy thông số like, share, comment cho post cũ
        $schedule->command('v3getoldpostdata:list all')->weekly(4)->withoutOverlapping();
        // Tính dữ liệu cho api
        $schedule->command('v3createhotfacedataforapi:list all')->dailyAt('14:00')->withoutOverlapping();
        // Đếm số follower của uid
        $schedule->command('v3getfollowerdaily:list all')->dailyAt('1:00')->withoutOverlapping();
        // Kiểm tra danh sách uid có khóa/xóa hay không
        $schedule->command('v3checkuidactive:list')->dailyAt('12:00')->withoutOverlapping();
        // Kiểm tra bị xóa đã mở lại chưa
        $schedule->command('v3checkinactiveuid:list')->dailyAt('16:00')->withoutOverlapping();
        // Kiểm tra bị xóa đã mở lại chưa
        $schedule->command('api:getUserFb')->weekly(4)->withoutOverlapping();
//        // Check follower FB
        $schedule->command('api:cronlistpost')->everyThirtyMinutes()->withoutOverlapping();
        // Lay post Data

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
