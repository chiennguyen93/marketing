<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Admin\ProcessingUsersController;
use GuzzleHttp\Exception\ClientException;
use App\Support\TokenHelper;

class cronlistpost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:cronlistpost';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get list post';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $Controllers;

    public function __construct(ProcessingUsersController $processingUsersController)
    {
        parent::__construct();
        $this->Controllers = $processingUsersController;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $this->Controllers->GetInfoPost();

        } catch (ClientException $e) {
            $this->access_token = TokenHelper::changeToken();
            \Log::debug($e->getMessage());
            return 0 ;
        }
    }
}
