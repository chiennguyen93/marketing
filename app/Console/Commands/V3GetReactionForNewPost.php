<?php

namespace App\Console\Commands;

use App\Jobs\V3GetPostReactionUser;
use App\Models\HotFace;
use App\Models\Post;
use Illuminate\Console\Command;

class V3GetReactionForNewPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'v3getreactionfornewpost:list {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lấy tương tác các post mới đăng trong 72h đầu';



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');
        if ($type == 'all') {
            $hotFaces = HotFace::all();
        } else {
            $hotFaces = HotFace::where('fb_uid', $type)->get();
        }
        $time       = datetime()->subDays(3)->toDateTimeString();
        $addMinutes = 1;
        foreach ($hotFaces as $key => $hotFace) {
            $posts = Post::where('hot_face_id', $hotFace->id)->where('fb_created', '>', $time)->get();
            foreach ($posts as $key => $post) {
                V3GetPostReactionUser::dispatch($post)->delay(now()->addMinutes($addMinutes));
                $addMinutes = $addMinutes + 1;
            }
        }
    }
}
