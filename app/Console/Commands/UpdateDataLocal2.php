<?php

namespace App\Console\Commands;

use App\Models\HotFace;
use App\Support\TokenHelper;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;
use App\Jobs\V3CheckAndGetPostData;

class UpdateDataLocal2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updatedatalocal2:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lấy post mới nhất của mỗi Uid hằng ngày';

    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    protected $access_token;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->access_token = TokenHelper::getToken();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $hotFaces = HotFace::where('post_count', '<', 10)->get();

        $start      = Carbon::now()->subDays(30)->getTimestamp();
        $end        = Carbon::now()->getTimestamp();
        $addMinutes = 1;
        foreach ($hotFaces as $key => $hotFace) {
            echo "Uid: ".$hotFace->fb_uid.PHP_EOL;
            try {
                $response = $this->httpClient()->get($hotFace->fb_uid . '/feed', [
                    'query' => [
                        'fields'       => 'id',
                        'since'        => $start,
                        'until'        => $end,
                        'access_token' => $this->access_token,
                    ],
                ]);
                $body       = $response->getBody();
                $data       = json_decode($body->getContents());
                $list_posts = $data->data;
                foreach ($list_posts as $key => $post) {
                    V3CheckAndGetPostData::dispatch($post->id, $hotFace->fb_uid, $hotFace->id);//->delay(now()->addMinutes($addMinutes));
                }
            } catch (ClientException $e) {
                $this->access_token = TokenHelper::changeToken();
                \Log::debug($e->getMessage());
                return 0;
            }
            $addMinutes = $addMinutes + 2;
        }
        echo "Done!".PHP_EOL;
    }

    /**
     * Get guzzle http client.
     *
     * @return \GuzzleHttp\Client
     */
    protected function httpClient()
    {
        $httpClient = new Client([
            'base_uri' => 'https://graph.facebook.com',
        ]);
        return $httpClient;
    }
}
