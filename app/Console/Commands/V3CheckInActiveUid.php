<?php

namespace App\Console\Commands;

use App\Models\HotFace;
use App\Support\TokenHelper;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;
use App\Jobs\V3CheckAndGetPostData;

class V3CheckInActiveUid extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'v3checkinactiveuid:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kiểm tra uid đã khóa xem đã mở lại chưa';

    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    protected $access_token;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->access_token = TokenHelper::getToken();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $hotFaces = HotFace::onlyTrashed()->get();

        foreach ($hotFaces as $key => $hotFace) {
            try {
                $response = $this->httpClient()->get($hotFace->fb_uid , [
                    'query' => [
                        'fields'       => 'id,name',
                        'access_token' => $this->access_token,
                    ],
                ]);
                $body       = $response->getBody();
                $data       = json_decode($body->getContents());
                if (isset($data->name)) {
                    $hotFace->name = $data->name;
                }
                $hotFace->restore();
                
            } catch (ClientException $e) {
                $this->access_token = TokenHelper::changeToken();
                \Log::debug($e->getMessage());
            }
        }

        echo "Done!".PHP_EOL;
    }

    /**
     * Get guzzle http client.
     *
     * @return \GuzzleHttp\Client
     */
    protected function httpClient()
    {
        if($this->httpClient) {
            return $this->httpClient;
        }else {
            $this->httpClient = new Client([
                'base_uri' => 'https://graph.facebook.com',
            ]);
        }
        
        return $this->httpClient;
    }
}
