<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\HotFace;
use App\Jobs\V3CountFollowerOfHotFace;

class V3GetFollowerDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'v3getfollowerdaily:list {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');

        if ($type == 'all') {
            $hotFaces = HotFace::all();
        } else {
            $hotFaces = HotFace::where('fb_uid', $type)->get();
        }
        $addMinutes = 1;
        foreach ($hotFaces as $key => $hotFace) {
            V3CountFollowerOfHotFace::dispatch($hotFace)->delay(now()->addMinutes($addMinutes));
            $addMinutes = $addMinutes + 1;
        }
    }
}
