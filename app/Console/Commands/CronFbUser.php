<?php
/**
 * Created by PhpStorm.
 * User: tu
 * Date: 10/08/2018
 * Time: 13:37
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\API\v1\HomeController;

class CronFbUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:getUserFb';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $homecontrollers;
    public function __construct(HomeController $homeController)
    {
        parent::__construct();

        $this->homecontrollers = $homeController;
    }
    protected $convert = true;
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->homecontrollers->getall();
    }
}