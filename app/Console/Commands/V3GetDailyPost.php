<?php

namespace App\Console\Commands;

use App\Jobs\V3JobGetDailyPost;
use App\Models\HotFace;
use App\Support\TokenHelper;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Carbon\Carbon;

class V3GetDailyPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'v3getdailypost:list {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lấy post mới nhất của mỗi Uid hằng ngày';

    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    protected $access_token;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->access_token = TokenHelper::getToken();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');

        if ($type == 'all') {
            $hotFaces = HotFace::all();
        } else {
            $hotFaces = HotFace::where('fb_uid', $type)->get();
        }
        $start      = Carbon::now()->subDays(1)->getTimestamp();
        $end        = Carbon::now()->getTimestamp();
        $addMinutes = 1;
        foreach ($hotFaces as $key => $hotFace) {
            V3JobGetDailyPost::dispatch($hotFace, $start, $end)->delay(now()->addMinutes($addMinutes));
            $addMinutes = $addMinutes + 1;
        }
    }

    /**
     * Get guzzle http client.
     *
     * @return \GuzzleHttp\Client
     */
    protected function httpClient()
    {
        $httpClient = new Client([
            'base_uri' => 'https://graph.facebook.com',
        ]);
        return $httpClient;
    }
}
