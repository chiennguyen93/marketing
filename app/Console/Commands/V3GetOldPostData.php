<?php

namespace App\Console\Commands;

use App\Jobs\V3CheckAndGetPostData;
use App\Models\HotFace;
use App\Models\Post;
use Illuminate\Console\Command;

class V3GetOldPostData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'v3getoldpostdata:list {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lấy số like, cmt, share các post';



    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');
        if ($type == 'all') {
            $hotFaces = HotFace::all();
        } else {
            $hotFaces = HotFace::where('fb_uid', $type)->get();
        }
        $addMinutes = 1;
        foreach ($hotFaces as $key => $hotFace) {
            $posts = Post::where('hot_face_id', $hotFace->id)->orderBy('fb_created', 'desc')->limit(intval($hotFace->number_post))->get();
            foreach ($posts as $key => $post) {
                V3CheckAndGetPostData::dispatch($post->fb_id, $hotFace->fb_uid, $hotFace->id)->delay(now()->addMinutes($addMinutes));
                $addMinutes = $addMinutes + 1;
            }
        }
    }
}
