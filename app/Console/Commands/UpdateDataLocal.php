<?php

namespace App\Console\Commands;

use App\Models\Post;
use App\Models\HotFace;
use Illuminate\Console\Command;

class UpdateDataLocal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updatedatalocal:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    protected $convert = true;
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $hotFaces = HotFace::where('post_count', '<', 10)->orWhereNull('post_count')->get();
        foreach ($hotFaces as $key => $hotFace) {
            echo "Update hotface uid: ".$hotFace->fb_uid.PHP_EOL;
            $hotFace->post_count = $hotFace->posts->count();
            $hotFace->save();
        }

    }

}
