<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\V3JobCreateHotFaceDataForApi;
use App\Models\HotFace;
use App\Models\Post;

class V3CreateHotFaceDataForApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'v3createhotfacedataforapi:list {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tính thông tin trả về qua api v3';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');
        if ($type == 'all') {
            $hotFaces = HotFace::all();
        } else {
            $hotFaces = HotFace::where('fb_uid', $type)->get();
        }
        $addMinutes = 1;
        foreach ($hotFaces as $key => $hotFace) {
            $posts = Post::where('hot_face_id', $hotFace->id)->orderBy('fb_created', 'desc')->limit(intval($hotFace->number_post))->get();
            V3JobCreateHotFaceDataForApi::dispatch($hotFace,$posts)->delay(now()->addMinutes($addMinutes));
            $addMinutes = $addMinutes + 1;
        }
    }
}
