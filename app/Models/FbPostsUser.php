<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FbPostsUser extends Eloquent
{
    use SoftDeletes;

    protected $table = 'fb_posts_user';

    protected $fillable = [
    ];
}
