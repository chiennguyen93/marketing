<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Token extends Eloquent
{
    use SoftDeletes;

    protected $table = 'tokens';

    protected $fillable = [
    	'content','status'
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = [
        'content' => 'required|unique:tokens'
    ];

}
