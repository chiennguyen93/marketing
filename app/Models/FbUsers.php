<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class FbUsers extends Eloquent
{
    use SoftDeletes;

    protected $table = 'fb_users_all';

    protected $fillable = [
    ];

}
