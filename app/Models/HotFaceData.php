<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class HotFaceData extends Eloquent
{
    use SoftDeletes;

    protected $table = 'fb_hot_face_datas';

    protected $fillable = [
    ];

}
