<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class HotFaceRaw extends Eloquent
{
    use SoftDeletes;

    protected $table = 'fb_hot_faces_raw';

    protected $fillable = [
    ];
}
