<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Jobs\V3GetPostReactionUser;

class Post extends Eloquent
{
    use SoftDeletes;

    protected $table = 'posts';

    protected $fillable = [
    ];

    protected $appends = [
        'total_like'
    ];

    public function getTotalLikeAttribute ($value) {
        return $this->count_likes + $this->count_loves + $this->count_wows + $this->count_hahas + $this->count_sads + $this->count_angrys + $this->count_thankfuls;
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            V3GetPostReactionUser::dispatch($model);
        });
        
    }
}
