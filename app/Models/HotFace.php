<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Jobs\V3CreateDataForNewUid;

class HotFace extends Eloquent
{
    use SoftDeletes;

    protected $table = 'hot_faces';

    protected $fillable = [
        'name',
        'fb_uid',
        'number_post',
        'sync_like'
    ];

    public static $rules = [
        'fb_uid' => 'required|unique:hot_faces'
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            V3CreateDataForNewUid::dispatch($model);
        });
        
    }

    public function posts() {
        return $this->hasMany(Post::class);
    }
}
