<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class PostReactionFbUser extends Eloquent
{
    use SoftDeletes;

    protected $table = 'post_reaction_fb_users';

    protected $fillable = [
    ];

    public function fbUser() {
        return $this->belongsTo(FbUser::class, 'fb_user_id');
    }
}
