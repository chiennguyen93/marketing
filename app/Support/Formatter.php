<?php

namespace App\Support;

use Carbon\Carbon;
use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money;
use Money\MoneyFormatter;

class Formatter
{
    /**
     * @var string
     */
    const LOCALE = 'en_US';

    /**
     * @var string
     */
    const TIME_FORMAT = 'H:i';

    /**
     * Get the money instance.
     *
     * @param  mixed $amount
     * @param  string $currency
     * @return \Money\Money
     */
    public function money($amount, $currency)
    {
        return new Money($amount, new Currency($currency));
    }

    /**
     * Formatting the money.
     *
     * @param  \Money\Money $money
     * @param  \Money\MoneyFormatter|null $formatter
     * @param  string $locale
     * @return string
     */
    public function moneyFormat(Money $money, MoneyFormatter $formatter = null)
    {
        if (is_null($formatter)) {
            $numberFormatter = new \NumberFormatter(static::LOCALE, \NumberFormatter::CURRENCY);
            $formatter = new IntlMoneyFormatter($numberFormatter, new ISOCurrencies);
        }

        return $formatter->format($money);
    }

    /**
     * Formatting time range.
     *
     * @param  mixed $start
     * @param  mixed $end
     * @param  string $rangeFormat
     * @return string
     */
    public function timeRage($start, $end, string $rangeFormat = '%s-%s')
    {
        $format = function ($time) {
            if ($time instanceof Carbon) {
                return $time->format(static::TIME_FORMAT);
            }
            return $time;
        };

        return sprintf($rangeFormat, $format($start), $format($end));
    }
}
