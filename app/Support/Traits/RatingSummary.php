<?php

namespace App\Support\Traits;

trait RatingSummary
{
    /**
     * Describe rating summary.
     *
     * @param  float $rating
     * @return string
     */
    public function describeRatingSummary($rating)
    {
        if ($rating < 1) {
            return 'Rất kém';
        } elseif ($rating >= 1 && $rating < 2) {
            return 'Kém';
        } elseif ($rating >= 2 && $rating < 3) {
            return 'Trung bình';
        } elseif ($rating >= 3 && $rating < 4) {
            return 'Tốt';
        } elseif ($rating >= 4 && $rating <= 5) {
            return 'Rất tốt';
        } else {
            return '';
        }
    }
}
