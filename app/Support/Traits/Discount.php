<?php

namespace App\Support\Traits;

trait Discount
{
    /**
     * Get the discounted value.
     *
     * @param  mixed $cost
     * @param  int $value
     * @param  string $type
     * @return mixed
     */
    public function discountWorth($cost, $value, $type = '')
    {
        switch ($type) {
            case 'percentage':
            case 'percent':
            case '%':
                return $cost * $value / 100;
                break;

            default:
                return $value;
                break;
        }
    }

    /**
     * Get the final cost after discount.
     *
     * @param  mixed $cost
     * @param  int $value
     * @param  string $type
     * @return mixed
     */
    public function discountCost($cost, $value, $type = '')
    {
        return $cost - $this->discountValue($cost, $value, $type);
    }
}
