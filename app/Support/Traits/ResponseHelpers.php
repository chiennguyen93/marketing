<?php

namespace App\Support\Traits;

use App\Models\Booking;
use App\Models\Reservation;
use App\Models\Salon;
use App\Models\Stylist;
use App\Serializers\ArraySerializer;
use App\Transformers\BookingTransformer;
use App\Transformers\StylistTransformer;
use Facades\App\Services\Booking as BookingFactory;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use League\Fractal\Serializer\SerializerAbstract;
use Spatie\Fractalistic\Fractal;

trait ResponseHelpers
{
    /**
     * Use default array serializer if a request parameter is set.
     *
     * @param  \Spatie\Fractalistic\Fractal $fractal
     * @param  \League\Fractal\Serializer\SerializerAbstract|null $serializer
     * @param  string $key
     * @return \Spatie\Fractalistic\Fractal
     */
    public function useArraySerializer(Fractal $fractal, SerializerAbstract $serializer = null, $key = '_meta')
    {
        if (request($key)) {
            $fractal->serializeWith($serializer ?: new ArraySerializer);
        }

        return $fractal;
    }

    /**
     * Fetch data and return the response.
     *
     * @param  \App\Models\Salon $salon
     * @return array
     */
    public function getProfileResponse(Salon $salon)
    {
        $stylists = $salon->stylists();

        $stylist_id = request('stylist_id');

        if ($stylist_id) {
            $stylists->where('stylist_id', '=', $stylist_id);
        }

        $stylists = $stylists->get();

        return [
            'stylists' => [
                'count' => $stylists->count(),
                'data' => fractal($stylists, new StylistTransformer)->toArray(),
            ],
            'bookings' => [
                'incoming' => $salon->bookings()
                    ->with('reservations')
                    ->whereHas('reservations', function ($q) use ($stylist_id) {
                        $q->where('end_date', '>=', datetime());
                        if ($stylist_id) {
                            $q->where('stylist_id', '=', $stylist_id);
                        }
                    })
                    ->count(),
                'completed' => $salon->bookings()
                    ->with('reservations')
                    ->whereHas('reservations', function ($q) use ($stylist_id) {
                        $q->where('end_date', '<', datetime());
                        if ($stylist_id) {
                            $q->where('stylist_id', '=', $stylist_id);
                        }
                    })
                    ->count(),
                'canceled' => $salon->bookings()->whereHas('reservations', function ($q) use ($stylist_id) {
                    if ($stylist_id) {
                        $q->where('stylist_id', '=', $stylist_id);
                    }
                })->onlyTrashed()->count(),
            ],
            'services' => [
                'count' => $salon->services()->whereHas('stylists', function ($q) use ($stylist_id) {
                    if ($stylist_id) {
                        $q->where('stylist_id', '=', $stylist_id);
                    }
                })->count()
            ]
        ];
    }

    /**
     * Enable stylist schedule slots then return the response.
     *
     * @param  \App\Models\Stylist $stylist
     * @return \Illuminate\Http\Response
     */
    public function enableStylistSchedule(Stylist $stylist)
    {
        foreach ($this->getSlotsFromRequest() as $slot) {
            $start = array_get($slot, 'start');
            $end = array_get($slot, 'end');

            if ($start && $end) {
                $reservation = $stylist->reservations()
                    ->where('start_date', '=', $start)
                    ->where('end_date', '=', $end)
                    ->where('type', '=', Reservation::TYPE_DISABLED)
                    ->first();

                if ($reservation) {
                    $reservation->forceDelete();
                }
            }
        }

        return response()->noContent();
    }

    /**
     * Disable stylist schedule slots then return the response.
     *
     * @param  \App\Models\Stylist $stylist
     * @return \Illuminate\Http\Response
     */
    public function disableStylistSchedule(Stylist $stylist)
    {
        foreach ($this->getSlotsFromRequest() as $slot) {
            $start = array_get($slot, 'start');
            $end = array_get($slot, 'end');

            if ($start && $end) {
                $stylist->reservations()->save(new Reservation([
                    'start_date' => $start,
                    'end_date' => $end,
                    'type' => Reservation::TYPE_DISABLED,
                ]));
            }
        }

        return response()->noContent();
    }

    /**
     * Delete booking then return the response.
     *
     * @param  \App\Models\Booking $booking
     * @return \Illuminate\Http\Response
     */
    public function deleteBookingAndResponse(Booking $booking)
    {
        if ($reason = request('reason')) {
            $booking->update(['cancellation_reason' => $reason]);
        }

        $booking->delete();

        return response()->noContent();
    }

    /**
     * Get and parse slots param from request.
     *
     * @return array
     */
    protected function getSlotsFromRequest()
    {
        $request = request();

        $this->validate($request, [
            'slots' => 'required|json',
        ]);

        $slots = json_decode($request->slots, true);

        if (! is_array($slots)) {
            return response()->error(400, __('validation.regex', ['attribute' => 'slots']));
        }

        return $slots;
    }

    /**
     * Get salon bookings query.
     *
     * @param  \App\Models\Salon $salon
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getSalonBookingsQuery(Salon $salon)
    {
        return $salon->bookings()
            ->select('bookings.*')
            ->join('reservations', 'bookings.id', '=', 'reservations.booking_id')
            ->oldest('reservations.start_date');
    }

    /**
     * Format bookings reponse.
     *
     * @param  \Illuminate\Contracts\Pagination\LengthAwarePaginator $bookings
     * @param  array $includes
     * @return \Illuminate\Http\Response
     */
    public function getSalonBookingsResponse(LengthAwarePaginator $bookings, $includes = [], array $meta = [])
    {
        return response()->fractal(request('_meta') ? $bookings : $bookings->getCollection(), (new BookingTransformer)->scope('dates'), function ($fractal) use ($includes, $meta) {
            return $this->useArraySerializer($fractal->parseIncludes($includes)->addMeta($meta));
        });
    }

    /**
     * Create new booking.
     *
     * @param  \App\Models\Salon $salon
     * @param  array $data
     * @param  mixed $user
     * @return \Illuminate\Http\Response
     */
    public function newBooking(Salon $salon, $user, array $data = [])
    {
        $request = request();
        $booking = BookingFactory::user($user)
            ->time($request->time);
        $slots = json_decode($request->slots, true);

        if (! is_array($slots)) {
            return response()->error(400, __('validation.regex', ['attribute' => 'slots']));
        }

        foreach ($slots as $slot) {
            $service = array_get($slot, 'service');
            $stylist = array_get($slot, 'stylist');
            // $date = array_get($slot, 'date');
            $note = array_get($slot, 'note');

            if ($service && $stylist) {
                $booking->service($service)
                    ->stylist($stylist)
                    ->reserve($note);
            }
        }

        if ($request->phone) {
            $data = [
                'country_code' => 'VN',
                'phone_number' => $request->phone,
            ];
        }

        $booking = $booking->create($salon, $data);

        return response()->created();
    }
}
