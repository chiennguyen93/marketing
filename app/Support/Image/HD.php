<?php

namespace App\Support\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class HD implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(1280, 720);
    }
}
