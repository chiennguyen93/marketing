<?php

namespace App\Support\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class Square300 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(300, 300);
    }
}
