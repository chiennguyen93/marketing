<?php

namespace App\Support\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class ImageBlog implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(1000, 500);
    }
}
