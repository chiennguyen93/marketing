<?php

namespace App\Support\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class FullHD implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(1920, 1080);
    }
}
