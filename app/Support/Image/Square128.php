<?php

namespace App\Support\Image;

use Intervention\Image\Filters\FilterInterface;
use Intervention\Image\Image;

class Square128 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(128, 128);
    }
}
