<?php

namespace App\Support;
use App\Models\Token;
use Cache;

class TokenHelper
{
    public static function getToken() {
        $value = Cache::remember('currentToken', 1000, function () {
            $token = Token::orderBy('updated_at','asc')->first();
            if ($token) {
                return $token->content;
            } else {
                return null;
            }
        });
        return $value;
    }

    public static function changeToken() {
        $oldContent = static::getToken();
        \Log::info("Change token: ". $oldContent);
        $token = Token::where('content',$oldContent)->first();
        $token->error_at = datetime()->toDateTimeString();
        $token->save();
        Cache::forget('currentToken');
        return static::getToken();
    }
}
