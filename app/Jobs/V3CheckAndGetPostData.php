<?php

namespace App\Jobs;

use App\Models\Post;
use App\Support\TokenHelper;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class V3CheckAndGetPostData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;

    protected $access_token;
    protected $post_id;
    protected $fb_uid;
    protected $hot_face_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($post_id, $fb_uid, $hot_face_id)
    {
        $this->post_id      = $post_id;
        $this->fb_uid       = $fb_uid;
        $this->hot_face_id  = $hot_face_id;
    }

    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    /**
     * Get guzzle http client.
     *
     * @return \GuzzleHttp\Client
     */
    protected function httpClient()
    {
        $httpClient = new Client([
            'base_uri' => 'https://graph.facebook.com',
        ]);
        return $httpClient;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        $this->access_token = TokenHelper::getToken();
        $post = Post::where('fb_id', $this->post_id)->first();
        if (!$post) {
            $post = new Post();
        }
        $post->fb_id       = $this->post_id;
        $post->hot_face_id = $this->hot_face_id;
        $post->fb_uid      = $this->fb_uid;
        $this->getPostData($post->fb_id, $post);

    }

    protected function getPostData($fbPostId, $post)
    {
        try {

            $response = $this->httpClient()->get('/v3.0/' . $fbPostId, [
                'query' => [
                    'fields'       => 'id,created_time,type,message,comments,shares,attachments',
                    'access_token' => $this->access_token,
                ],
            ]);
            $body = $response->getBody();
            $data = json_decode($body->getContents());
            if (isset($data->message)) {
                $post->message = $data->message;
            } else {
                $post->message = "Trống";
            }
            $post->type           = $data->type ?: 0;
            $date                 = new Carbon($data->created_time);
            $post->fb_created     = $date->addHours(7)->toDateTimeString();
            $post->count_comments = 0;
            $post->count_shares   = 0;
            if (isset($data->comments)) {
                $post->count_comments = $data->comments->count;
            }
            if (isset($data->shares)) {
                $post->count_shares = $data->shares->count;
            }
            $post->count_likes     = $this->getReactionCount($fbPostId, 'LIKE');
            $post->count_loves     = $this->getReactionCount($fbPostId, 'LOVE');
            $post->count_wows      = $this->getReactionCount($fbPostId, 'WOW');
            $post->count_hahas     = $this->getReactionCount($fbPostId, 'HAHA');
            $post->count_sads      = $this->getReactionCount($fbPostId, 'SAD');
            $post->count_angrys    = $this->getReactionCount($fbPostId, 'ANGRY');
            $post->count_thankfuls = $this->getReactionCount($fbPostId, 'THANKFUL');

            $arr_attachments = [];
            if (isset($data->attachments) && $post->type == 'photo') {
                foreach ($data->attachments->data as $key => $attachment) {
                    if (isset($attachment->subattachments)) {
                        foreach ($attachment->subattachments->data as $key => $subattachment) {
                            if (isset($subattachment->target)) {
                                $arr_attachments[] = $subattachment->target->url;
                            }
                        }
                    } else {
                        if (isset($attachment->target)) {
                            $arr_attachments[] = $attachment->target->url;
                        }
                    }
                }
            }
            $post->attachments = $arr_attachments;
            $post->save();
        } catch (ClientException $e) {
            $this->access_token = TokenHelper::changeToken();
            \Log::debug($e->getMessage());
        }
    }

    protected function getReactionCount($fbPostId, $type = 'LIKE')
    {
        try {

            $response = $this->httpClient()->get($fbPostId . '/reactions', [
                'query' => [
                    'limit'        => 1,
                    'summary'      => 'total_count',
                    'type'         => $type,
                    'access_token' => $this->access_token,
                ],
            ]);
            $body = $response->getBody();
            $data = json_decode($body->getContents());
            return $data->summary->total_count;
        } catch (ClientException $e) {
            $this->access_token = TokenHelper::changeToken();
            \Log::debug($e->getMessage());
            return 0;
        }
    }

}
