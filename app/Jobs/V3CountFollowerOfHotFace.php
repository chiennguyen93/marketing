<?php

namespace App\Jobs;

use App\Models\HotFace;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use GuzzleHttp\Client;
use App\Support\TokenHelper;
use GuzzleHttp\Exception\ClientException;

class V3CountFollowerOfHotFace implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    
    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;

    protected $hotFace;
    protected $access_token;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($hotFace)
    {
        $this->hotFace      = $hotFace;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        try {
            $this->access_token = TokenHelper::getToken();
            $response = $this->httpClient()->get($this->hotFace->fb_uid . '/subscribers', [
                'query' => [
                    'limit'        => 1,
                    'access_token' => $this->access_token,
                ],
            ]);
            $body          = $response->getBody();
            $data          = json_decode($body->getContents());
            $this->hotFace->count_followers = $data->summary->total_count;
            $this->hotFace->save();
        } catch (ClientException $e) {
            $this->access_token = TokenHelper::changeToken();
            \Log::debug($e->getMessage());
            return 0;
        }
    }

    /**
     * Get guzzle http client.
     *
     * @return \GuzzleHttp\Client
     */
    protected function httpClient()
    {
        $httpClient = new Client([
            'base_uri' => 'https://graph.facebook.com/v1.0',
        ]);

        return $httpClient;
    }
}
