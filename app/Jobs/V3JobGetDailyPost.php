<?php

namespace App\Jobs;

use App\Support\TokenHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class V3JobGetDailyPost implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $hotFace;
    protected $access_token;
    protected $start;
    protected $end;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($hotFace, $start, $end)
    {
        $this->access_token = TokenHelper::getToken();
        $this->hotFace      = $hotFace;
        $this->start        = $start;
        $this->end          = $end;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $addMinutes = 1;
        try {
            $response = $this->httpClient()->get($this->hotFace->fb_uid . '/feed', [
                'query' => [
                    'fields'       => 'id',
                    'since'        => $this->start,
                    'until'        => $this->end,
                    'access_token' => $this->access_token,
                ],
            ]);
            $body       = $response->getBody();
            $data       = json_decode($body->getContents());
            $list_posts = $data->data;
            foreach ($list_posts as $key => $post) {
                V3CheckAndGetPostData::dispatch($post->id, $this->hotFace->fb_uid, $this->hotFace->id)->delay(now()->addMinutes($addMinutes));
                $addMinutes = $addMinutes + 1;
            }
        } catch (ClientException $e) {
            $this->access_token = TokenHelper::changeToken();
            \Log::debug($e->getMessage());
            return 0;
        }
    }

    /**
     * Get guzzle http client.
     *
     * @return \GuzzleHttp\Client
     */
    protected function httpClient()
    {
        $httpClient = new Client([
            'base_uri' => 'https://graph.facebook.com',
        ]);
        return $httpClient;
    }
}
