<?php

namespace App\Jobs;

use App\Models\FbLocation;
use App\Models\FbUser;
use App\Models\PostReactionFbUser;
use App\Support\TokenHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class V3GetPostReactionUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient = null;

    protected $access_token;

    protected $data = [];

    protected $next_url = '';

    protected $post;

    /**
     * Get guzzle http client.
     *
     * @return \GuzzleHttp\Client
     */
    protected function httpClient()
    {
        if($this->httpClient) {
            return $this->httpClient;
        }else {
            $this->httpClient = new Client([
                'base_uri' => 'https://graph.facebook.com',
            ]);
        }
        
        return $this->httpClient;
    }

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($post)
    {
        $this->post         = $post;
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->access_token = TokenHelper::getToken();
        try {
            $response = $this->httpClient()->get('/v3.0/' . $this->post->fb_id . '/reactions', [
                'query' => [
                    'fields'       => 'id,type',
                    'limit'        => 100,
                    'access_token' => $this->access_token,
                ],
            ]);
            $body       = $response->getBody();
            $data       = json_decode($body->getContents());
            $this->data = array_merge($this->data, $data->data);
            if (isset($data->paging->next)) {
                $this->next_url = $data->paging->next;
            } else {
                $this->next_url = '';
            }
            while ($this->next_url) {
                $this->getReactionPart($this->next_url);
            }
            $this->checkAndInsertPostFbUser($this->data, $this->post);
        } catch (ClientException $e) {
            $this->access_token = TokenHelper::changeToken();
            \Log::debug($e->getMessage());
        }
    }

    protected function getReactionPart($url)
    {
        try {
            $response = $this->httpClient()->get($url);
            $body     = $response->getBody();
            $data     = json_decode($body->getContents());

            $this->data = array_merge($this->data, $data->data);
            if (isset($data->paging->next)) {
                $this->next_url = $data->paging->next;
            } else {
                $this->next_url = '';
            }
        } catch (ClientException $e) {
            $this->access_token = TokenHelper::changeToken();
            \Log::debug($e->getMessage());
        }

    }

    protected function checkAndInsertPostFbUser($data = [])
    {
        foreach ($data as $key => $item) {
            $postReactionFbUser = PostReactionFbUser::where('post_id', $this->post->id)->where('fb_user_uid', $item->id)->first();
            if (!$postReactionFbUser) {
                $postReactionFbUser              = new PostReactionFbUser();
                $postReactionFbUser->post_id     = $this->post->id;
                $postReactionFbUser->post_fb_id  = $this->post->fb_id;
                $postReactionFbUser->hot_face_id = $this->post->hot_face_id;
                $postReactionFbUser->fb_user_uid = $item->id;

                // thêm mới
                $fbUser  = $this->checkAndCreatFbUser($postReactionFbUser->fb_user_uid);
                if ($fbUser) {
                    $postReactionFbUser->fb_user_id = $fbUser->id;
                    $postReactionFbUser->gender = isset($fbUser->gender) ? $fbUser->gender : null;
                    $postReactionFbUser->location_id = isset($fbUser->location_id) ? $fbUser->location_id : null;
                    $postReactionFbUser->year_of_birth = isset($fbUser->year_of_birth) ? $fbUser->year_of_birth : null;
                }
                
                $postReactionFbUser->update_time = 1;

                $postReactionFbUser->type        = $item->type;
                $postReactionFbUser->save();
            }
        }
    }

    protected function checkAndCreatFbUser($fb_uid)
    {
        $fbUser = FbUser::where('fb_uid', $fb_uid)->first();
        if (!$fbUser) {
            try {
                $response = $this->httpClient()->get('/v3.0/' . $fb_uid, [
                    'query' => [
                        'fields'       => 'name,gender,birthday,email,mobile_phone,location,address',
                        'access_token' => $this->access_token,
                    ],
                ]);
                $body = $response->getBody();
                $data = json_decode($body->getContents());

                $fbUser         = new FbUser();
                $fbUser->fb_uid = $fb_uid;
                if (isset($data->name)) {
                    $fbUser->name = $data->name;
                }
                if (isset($data->gender)) {
                    $fbUser->gender = $data->gender;
                }
                if (isset($data->email)) {
                    $fbUser->email = $data->email;
                }
                if (isset($data->mobile_phone)) {
                    $fbUser->mobile_phone = $data->mobile_phone;
                }
                if (isset($data->address)) {
                    $fbUser->address = $data->address;
                }
                if (isset($data->birthday)) {
                    if (strlen($data->birthday) == 10) {
                        list($yyyy, $mm, $dd) = explode('/', $data->birthday);
                        $error                = false;
                        if (!checkdate($yyyy, $mm, $dd)) {
                            $error = true;
                        }
                        if (!$error) {
                            $fbUser->birthday      = $data->birthday;
                            $fbUser->year_of_birth = $this->getAgeRange($data->birthday);
                        }
                    }
                }
                if (isset($data->location)) {
                    $fbUser->location_id = $data->location->id;
                    $this->checkAndCreatFbLocation($fbUser->location_id);
                }
                $fbUser->save();
            } catch (ClientException $e) {
                $this->access_token = TokenHelper::changeToken();
                \Log::debug($e->getMessage());
                return null;
            }
        }
        return $fbUser;
    }

    protected function getAgeRange($birthday)
    {
        $arr = explode('/', $birthday);
        return $arr[2];
    }

    protected function checkAndCreatFbLocation($location_id)
    {
        $fbLocation = FbLocation::where('fb_id', $location_id)->first();
        if (!$fbLocation) {
            try {
                $response = $this->httpClient()->get('/v3.0/' . $location_id, [
                    'query' => [
                        'fields'       => 'id,name',
                        'access_token' => $this->access_token,
                    ],
                ]);
                $body = $response->getBody();
                $data = json_decode($body->getContents());

                $fbLocation               = new FbLocation();
                $fbLocation->fb_id        = $data->id;
                $fbLocation->name         = $data->name;
                $fbLocation->name_unicode = $this->stripUnicode($fbLocation->name);
                $fbLocation->save();
            } catch (ClientException $e) {
                $this->access_token = TokenHelper::changeToken();
                \Log::debug($e->getMessage());
                return null;
            }

        }
    }

    protected function stripUnicode($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', ' ', $str);
        return $str;
    }
}
