<?php

namespace App\Jobs;

use App\Support\TokenHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;

class V3CreateDataForNewUid implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $httpClient;
    protected $hotface;
    protected $access_token;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($hotface)
    {
        $this->hotface = $hotface;
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {   
        $this->access_token = TokenHelper::getToken();
        $this->hotface->count_followers = $this->getSubscribers();
        $this->getListPost();
        $this->hotface->save();
    }

    protected function getSubscribers()
    {
        try {
            $response = $this->httpClient()->get('https://graph.facebook.com/v1.0/' . $this->hotface->fb_uid . '/subscribers', [
                'query' => [
                    'limit'        => 1,
                    'access_token' => $this->access_token,
                ],
            ]);
            $body = $response->getBody();
            $data = json_decode($body->getContents());
            if (isset($data->summary)) {
                if (isset($data->summary->total_count)) {
                    return $data->summary->total_count;
                }
            }
            return 0;
        } catch (ClientException $e) {
            $this->access_token = TokenHelper::changeToken();
            \Log::debug($e->getMessage());
            return 0;
        }
    }

    /**
     * Get guzzle http client.
     *
     * @return \GuzzleHttp\Client
     */
    protected function httpClient()
    {
        $httpClient = new Client([
            'base_uri' => 'https://graph.facebook.com/v3.0',
        ]);

        return $httpClient;
    }

    protected function getListPost() {
        try {
            $start = Carbon::now()->subDays(90)->getTimestamp();
            $end   = Carbon::now()->getTimestamp();
            $response = $this->httpClient()->get($this->hotface->fb_uid . '/posts', [
                'query' => [
                    'fields'       => 'id',
                    'since'        => $start,
                    'until'        => $end,
                    'access_token' => $this->access_token,
                ],
            ]);
            $body       = $response->getBody();
            $data       = json_decode($body->getContents());
            $list_posts = $data->data;

            $addMinutes = 1;
            foreach ($list_posts as $key => $post) {
                V3CheckAndGetPostData::dispatch($post->id, $this->hotface->fb_uid, $this->hotface->id)->delay(now()->addMinutes($addMinutes));
                $addMinutes = $addMinutes + 1;
            }
            return 0;
        } catch (ClientException $e) {
            $this->access_token = TokenHelper::changeToken();
            \Log::debug($e->getMessage());
            return 0;
        }
    }
}
