<?php

namespace App\Jobs;

use App\Models\FbLocation;
use App\Models\HotFace;
use App\Models\HotFaceData;
use App\Models\PostReactionFbUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class V3JobCreateHotFaceDataForApi implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($hotFace, $posts)
    {
        $this->hotFace = $hotFace;
        $this->posts   = $posts;
    }
    protected $hotFace;
    protected $posts;
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $hotFaceData = HotFaceData::where('hot_face_id', $this->hotFace->id)->first();
        if (!$hotFaceData) {
            $hotFaceData = new HotFaceData();
        }
        $hotFaceData->hot_face_id      = $this->hotFace->id;
        $hotFaceData->gender           = $this->getGender($this->hotFace, $this->posts);
        $hotFaceData->age_range        = $this->getAgeRanger($this->hotFace, $this->posts);
        $hotFaceData->location         = $this->getLocation2($this->hotFace, $this->posts);
        $hotFaceData->count_followers  = $this->hotFace->count_followers;
        $hotFaceData->like_per_post    = $this->getLikePerPost($this->hotFace, $this->posts);
        $hotFaceData->comment_per_post = $this->getCommentPerPost($this->hotFace, $this->posts);
        $hotFaceData->share_per_post   = $this->getSharePerPost($this->hotFace, $this->posts);
        $hotFaceData->save();
    }

    protected function getGender($hotFace, $posts)
    {
        $male_count = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->where('gender', 'male')
            ->count();
        $female_count = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->where('gender', 'female')
            ->count();
        $total_count = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->whereNotNull('gender')
            ->count();
        if ($total_count == 0) {
            $rs           = [];
            $rs['male']   = 0;
            $rs['female'] = 0;
            return $rs;
        }
        $rs           = [];
        $rs['male']   = round($male_count / $total_count * 100, 0, PHP_ROUND_HALF_UP);
        $rs['female'] = 100 - $rs['male'];
        return $rs;
    }

    protected function getLocation($hotFace, $posts)
    {
        // Ha Noi
        $location1 = FbLocation::where('name_unicode', 'regexp', '/ha noi/i')->orWhere('name_unicode', 'regexp', '/hanoi/i')->pluck('fb_id');
        $count1    = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->whereIn('location_id', $location1)
            ->count();
        // Hai Phong
        $location2 = FbLocation::where('name_unicode', 'regexp', '/hai phong/i')->pluck('fb_id');
        $count2    = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->whereIn('location_id', $location2)
            ->count();
        // Da Nang
        $location3 = FbLocation::where('name_unicode', 'regexp', '/da nang/i')->pluck('fb_id');
        $count3    = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->whereIn('location_id', $location3)
            ->count();
        // Ho Chi Minh
        $location4 = FbLocation::where('name_unicode', 'regexp', '/ho chi minh/i')->pluck('fb_id');
        $count4    = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->whereIn('location_id', $location4)
            ->count();

        // Can Tho
        $location5 = FbLocation::where('name_unicode', 'regexp', '/can tho/i')->pluck('fb_id');
        $count5    = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->whereIn('location_id', $location5)
            ->count();
        $total = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->count();
        if ($total == 0) {
            $rs                = [];
            $rs['Ha-Noi']      = 0;
            $rs['Hai-Phong']   = 0;
            $rs['Da-Nang']     = 0;
            $rs['Ho-Chi-Minh'] = 0;
            $rs['Can-Tho']     = 0;
            return $rs;
        }
        $rs                = [];
        $rs['Ha-Noi']      = round($count1 / $total * 100);
        $rs['Hai-Phong']   = round($count2 / $total * 100);
        $rs['Da-Nang']     = round($count3 / $total * 100);
        $rs['Ho-Chi-Minh'] = round($count4 / $total * 100);
        $rs['Can-Tho']     = round($count5 / $total * 100);
        return $rs;
    }
    protected function getAgeRanger($hotFace, $posts)
    {
        // 13-17
        $count1 = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->whereBetween('year_of_birth', ['2001', '2005'])
            ->count();
        //18-24
        $count2 = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->whereBetween('year_of_birth', ['1994', '2000'])
            ->count();
        //25-34
        $count3 = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->whereBetween('year_of_birth', ['1984', '1993'])
            ->count();
        //35-44
        $count4 = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->whereBetween('year_of_birth', ['1974', '1983'])
            ->count();
        //45+
        $count5 = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $posts->pluck('id')->toArray())
            ->where('year_of_birth', '<', '1974')
            ->count();
        $total = $count1 + $count2 + $count3 + $count4 + $count5;
        if ($total == 0) {
            $rs          = [];
            $rs['13-17'] = 0;
            $rs['18-24'] = 0;
            $rs['24-34'] = 0;
            $rs['35-44'] = 0;
            $rs['45+']   = 0;
            return $rs;
        }
        $rs          = [];
        $rs['13-17'] = round($count1 / $total * 100, 1);
        $rs['18-24'] = round($count2 / $total * 100, 1);
        $rs['24-34'] = round($count3 / $total * 100, 1);
        $rs['35-44'] = round($count4 / $total * 100, 1);
        $rs['45+']   = 100 - $rs['13-17'] - $rs['18-24'] - $rs['24-34'] - $rs['35-44'];
        if ($rs['45+'] < 0) {
            $rs['45+'] = 0;
        }
        return $rs;
    }

    protected function getLikePerPost($hotFace, $posts)
    {
        $total_like = 0;
        foreach ($posts as $key => $post) {
            $total_like = $total_like + $post->totalLike;
        }
        if ($posts->count() == 0) {
            return 0;
        }
        return round($total_like / $posts->count(), 0);
    }

    protected function getCommentPerPost($hotFace, $posts)
    {
        $total_comment = 0;
        foreach ($posts as $key => $post) {
            $total_comment = $total_comment + $post->count_comments;
        }
        if ($posts->count() == 0) {
            return 0;
        }
        return round($total_comment / $posts->count(), 0);
    }

    protected function getSharePerPost($hotFace, $posts)
    {
        $total_share = 0;
        foreach ($posts as $key => $post) {
            $total_share = $total_share + $post->count_shares;
        }
        if ($posts->count() == 0) {
            return 0;
        }
        return round($total_share / $posts->count(), 0);
    }

    protected function getLocation2($hotFace, $posts)
    {
        $postIds = $posts->pluck('id')->toArray();
        $arr = [
            'An Giang',
            'Vũng Tàu',
            'Bắc Giang',
            'Bắc Kạn',
            'Bạc Liêu',
            'Bắc Ninh',
            'Bến Tre',
            'Bình Định',
            'Bình Dương',
            'Bình Phước',
            'Bình Thuận',
            'Cà Mau',
            'Cao Bằng',
            'Đắk Lắk',
            'Đắk Nông',
            'Điện Biên',
            'Đồng Nai',
            'Đồng Tháp',
            'Gia Lai',
            'Hà Giang',
            'Hà Nam',
            'Hà Tĩnh',
            'Hải Dương',
            'Hậu Giang',
            'Hòa Bình',
            'Hưng Yên',
            'Khánh Hòa',
            'Kiên Giang',
            'Kon Tum',
            'Lai Châu',
            'Lâm Đồng',
            'Lạng Sơn',
            'Lào Cai',
            'Long An',
            'Nam Định',
            'Nghệ An',
            'Ninh Bình',
            'Ninh Thuận',
            'Phú Thọ',
            'Quảng Bình',
            'Quảng Nam',
            'Quảng Ngãi',
            'Quảng Ninh',
            'Quảng Trị',
            'Sóc Trăng',
            'Sơn La',
            'Tây Ninh',
            'Thái Bình',
            'Thái Nguyên',
            'Thanh Hóa',
            'Thừa Thiên Huế',
            'Tiền Giang',
            'Trà Vinh',
            'Tuyên Quang',
            'Vĩnh Long',
            'Vĩnh Phúc',
            'Yên Bái',
            'Phú Yên',
            'Cần Thơ',
            'Đà Nẵng',
            'Hải Phòng',
            'Hà Nội',
            'Hồ Chí Minh',
        ];
        $new_arr = [];
        foreach ($arr as $key => $item) {
            $new_arr[$item] = [
                'key1'        => $this->stripUnicode($item),
                'key2'        => $this->replaceSpace($this->stripUnicode($item)),
                'number_user' => 0,
            ];
        }
        $new_arr_2 = [];
        foreach ($new_arr as $key => $item) {
            $new_arr_2[$key] = $this->getLocationProvince($item, $postIds);
        }
        $total        = array_sum($new_arr_2);
        if ($total == 0) {
            $rs                = [];
            $rs['Hà Nội']      = 0;
            $rs['Hải Phòng']   = 0;
            $rs['Đà Nẵng']     = 0;
            $rs['Hồ Chí Minh'] = 0;
            $rs['Cần Thơ']     = 0;
            return $rs;
        }
        $sort_arr     = array_reverse(array_sort($new_arr_2));
        $caculate_arr = array_slice($sort_arr, 0, 5);
        $rs           = [];
        foreach ($caculate_arr as $key => $value) {
            $rs[$key] = round($value / $total * 100);
        }
        return $rs;
    }
    protected function stripUnicode($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', ' ', $str);
        return $str;
    }

    protected function replaceSpace($str)
    {
        $str = preg_replace('/([ ]+)/', '', $str);
        return $str;
    }

    protected function getLocationProvince($item, $postIds)
    {
        $locationIds = FbLocation::where('name_unicode', 'regexp', '/' . $item['key1'] . '/i')->orWhere('name_unicode', 'regexp', '/' . $item['key2'] . '/i')->pluck('fb_id');
        $count       = PostReactionFbUser::distinct('fb_user_id')->where('update_time', 1)->whereIn('post_id', $postIds)
            ->whereIn('location_id', $locationIds)
            ->count();
        return $count;
    }
}
