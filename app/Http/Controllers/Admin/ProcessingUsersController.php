<?php

namespace App\Http\Controllers\Admin;

use App\Models\FbFollowers;
use App\Models\FbPostsUser;
use App\Models\HotFace;
use App\Models\Post;

use App\Http\Controllers\Controller;

use App\Models\FbUsers;
use App\Models\HotFaceRaw;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use App\Support\TokenHelper;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use Illuminate\Http\Request;
class ProcessingUsersController extends Controller
{

   protected $access_token;
   protected $dataFollwers;
   protected $dataPosts;
   function __construct()
   {
       date_default_timezone_set('Asia/Ho_Chi_Minh');
       $this->access_token = TokenHelper::getToken();
       $this->dataFollwers = array();
       $this->dataPosts = 0;
   }


//    CROn CHECK USER THAT AO
   public function UserInfomation(){
       $Uid = '100006655331250';
       $this->getPosts($Uid);
       $this->checkPostsUser($Uid);
   }

//   CRON CHECKING USER FOLLWER CAO PHAN TICH SO TK > 5k FOllower

   public function checkUserFollwerhigh(){
      $Uid= '100004343254492';
      $this->getPosts($Uid);
      $this->checkPostUserFollowerFake($Uid);
   }


//   CRON GET INFO LIST POT
   public function GetInfoPost (){
       $post_id = ['100009353323514_2114105708911186','100010401075093_677800695909960','100012239954425_579324992485494','100004075672616_1493899057422602'];
       foreach ($post_id as $key => $value){
           $this->CallApiPost($value);
       }
   }


   public function exportAPI ($post_id) {
      $this->CallApiPost($post_id);
      $post = Post::where('fb_id', $post_id)->first();
       if (!$post) {
           return response()->json([
               'status'  => 404,
               'message' => 'Không tìm thấy Post',
               'data'    => null,
           ]);
       } else {
           $post->total_comment = $post->count_comments;
           $post->total_share = $post->count_shares;
           // $post->total_like = $post->count_likes + $post->count_loves + $post->count_wows + $post->count_hahas + $post->count_sads + $post->count_angrys + $post->count_thankfuls;
           $post->makeHidden(['_id', 'hot_face_id','fb_uid','count_comments', 'count_shares','count_likes','count_loves','count_wows','count_hahas','count_sads','count_angrys','count_thankfuls']);
           return response()->json([
               'status'  => 200,
               'message' => 'Success',
               'data'    => $post,
           ]);
       }
   }

// Get All follwers user
   protected function getFollowers($Uid){

       try {
          $data = $this->callbackNextFollower($Uid,false);
          $this->dataFollwers = [];
          return $this->saveFollowers($data,$Uid);

       } catch (ClientException $e) {
           $this->access_token = TokenHelper::changeToken();
           \Log::debug($e->getMessage());
           return 0 ;
       }
   }
// Get 50 posts user

   protected function getPosts($Uid){
        try {

            $data = $this->callbackNextPost($Uid,false);
            $this->dataPosts = 0;
            return $data;

        } catch (ClientException $e) {
            $this->access_token = TokenHelper::changeToken();
            \Log::debug($e->getMessage());
            return 0 ;
        }
    }

   protected function callbackNextFollower($Uid,$next){

       $client = new Client();

       ($next) ? $param  = [
           'after' => $next,
           'limit' => 5000,
           'access_token' => $this->access_token,
       ] : $param =[
           'limit' => 5000,
           'access_token' => $this->access_token,
       ];
       $response = $client->get('https://graph.facebook.com/v1.0/'. $Uid .'/subscribers', [
           'query' => $param,
       ]);


       $body = $response->getBody();
       $data = json_decode($body->getContents());


       if(isset($data->paging->next)){
            $this->dataFollwers = array_merge($this->dataFollwers, $data->data) ;
            return $this->callbackNextFollower($Uid,$data->paging->cursors->after);
       }else{
           return array_merge($this->dataFollwers, $data->data);
       }

   }

   protected function callbackNextPost($Uid,$next){
       $client = new Client();
       if($next){
           $response = $client->get($next);
       }else{
           $response = $client->get('https://graph.facebook.com/v1.0/'. $Uid .'/feed', [
               'query' => [
                   'limit' => 25,
                   'access_token' => $this->access_token,
               ],
           ]);
       }


       $body = $response->getBody();
       $data = json_decode($body->getContents());


       if(isset($data->paging->next)){

           foreach ($data->data as $key =>$val){
               $this->savePosts($val,$Uid);
               $this->dataPosts++;

               if($this->dataPosts > 50){
                   return true;
               }
           }
           return $this->callbackNextPost($Uid,$data->paging->next);
       }elseif(!isset($data->paging->next)){
           foreach ($data->data as $key =>$val){
               $this->savePosts($val,$Uid);
           }
           return true;
       }


   }

// POST API TAM thoi chay cron
   protected function CallApiPost ($post_id){
       $client = new Client();
       $response = $client->get('https://graph.facebook.com/v1.0/'. $post_id , [
           'query' => [
               'access_token' => $this->access_token,
           ],
       ]);

       $body = $response->getBody();
       $data = json_decode($body->getContents());


        $existsPost =  Post::where('fb_id',$post_id)->exists();
        if ($existsPost){
           return Post::where('fb_id',$post_id)->update(
                ['count_likes' => (isset($data->likes->count)) ? $data->likes->count : 0  ,
                 'count_shares' => (isset($data->shares->count)) ? $data->shares->count : 0 ,
                 'count_comments' =>  (isset($data->comments->count)) ? $data->comments->count : 0 ,
                ]);
        }else{


            $hotface = HotFace::where('fb_uid',$data->from->id)->first();

            $post = new Post();
            $post->fb_id = $post_id;
            $post->hot_face_id = $hotface->_id;
            $post->fb_uid = $data->from->id;
            $post->message = $data->message;
            $post->type = $data->type;
            $post->fb_created = $data->created_time;
            $post->count_comments=  (isset($data->comments->count)) ? $data->comments->count : 0;
            $post->count_shares=  (isset($data->shares->count)) ? $data->shares->count : 0 ;
            $post->count_likes=  (isset($data->likes->count)) ? $data->likes->count : 0;
            $post->count_loves=  0;
            $post->count_wows=  0;
            $post->count_hahas=  0;
            $post->count_sads=  0;
            $post->count_angrys=  0;
            $post->count_thankfuls = 0;
            $post->attachments = [];
           return $post ->save();
        }
   }



   protected function saveFollowers ($followers,$Uid){

       $arraySave = [];



       foreach ($followers as $key => $value){

           $dataExists =    FbFollowers::where('fb_uid','=',$value->id)->where('follower',$Uid )->exists();



           if (!$dataExists){
               $followers = new FbFollowers;
               $followers->fb_uid = $value->id;
               $followers->name = $value->name;
               $followers->follower = $Uid;
               return $followers->save();
           }
       }

   }

   protected function savePosts ($data,$Uid){


       $dataExists =  FbPostsUser::where('post.id','=',$data->id)->exists();

       if (!$dataExists){

           $db = new FbPostsUser;
           $db->Uid = $Uid;
           $db->post = $data;

           return $db->save();
       }

   }

   protected function checkPostsUser($Uid){
      $posts = FbPostsUser::where('Uid',$Uid)->get();

      $time2month = 60*24*60*60;
      $time1year  = 365*24*60*60;


      if($this->compareTime($posts[0]->post["updated_time"]) > $time2month) {
         $this->fakeUser($Uid);
      } elseif (count($posts) <= 30) {
         if ($this->compareTime($posts[count($posts) - 1]->post["updated_time"]) > $time1year){
             $this->fakeUser($Uid);
         }
      } else {
        $totalComment = 0;
        $totalLike = 0;
        $totalShare = 0;
        foreach ($posts as $key =>  $value){
           $post = $value->post;
           (isset($post['shares'])) ? $totalShare = $totalShare + $post['shares']['count'] : ""  ;
           (isset($post['likes'])) ? $totalLike = $totalLike + $post['likes']['count'] : ""  ;
           (isset($post['comments'])) ? $totalComment = $totalComment + $post['comments']['count'] : "";

        }
        $avgLike = $totalLike / count($posts);
        $avgComment = $totalComment / count($posts);
        $totalShare = $totalShare / count($posts);

        echo $avgLike;
        echo '<br/>';
        echo $avgComment;
        echo '<br/>';
        echo $totalShare;
        die;
        if($avgLike < 4 && $avgComment < 2){
            $this->fakeUser($Uid);
        }
      }
   }

   protected function compareTime($time) {

        $now = time();
        $time =  date_parse_from_format("Y-m-d H:i:s",$time);
        $compareTime =  $now -  mktime($time['hour'],$time['minute'],$time['second'],$time['month'],$time['day'],$time['year']);
        return $compareTime;
   }

//   update User fake in db Fb_users
   protected function fakeUser($Uid) {
       return FbUsers::where('fb_uid','=',$Uid)->update(['fake'=>1]);
   }



//    check FOllwer > 5k
   protected function FollowerFAKE($Uid) {
       return HotFaceRaw::where('fb_uid','=',$Uid)->update(['fake'=>1]);
    }


   protected function checkPostUserFollowerFake($Uid){
       $totalComment = 0;
       $totalLike = 0;
       $posts = FbPostsUser::where('Uid',$Uid)->get();

       foreach ($posts as $key =>  $value){
           $post = $value->post;
           (isset($post['likes'])) ? $totalLike = $totalLike + $post['likes']['count'] : ""  ;
           (isset($post['comments'])) ? $totalComment = $totalComment + $post['comments']['count'] : "";

       }
       $avgLike = $totalLike / count($posts);
       $avgComment = $totalComment / count($posts);


       if($avgLike < 300 && $avgComment < 30 ){
//           echo $Uid;
           $this->FollowerFAKE($Uid);
       }

   }

}
