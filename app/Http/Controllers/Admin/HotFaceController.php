<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\HotFace;
use App\Models\HotFaceData;
use App\Models\HotFaceRaw;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class HotFaceController extends Controller
{
    /**
     * Display a listing of the hotface.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $hotfaces = HotFace::withTrashed()->when($request->name, function ($q) use ($request) {
            $q->where('name', 'like', '%' . $request->name . '%')
                ->orWhere('fb_uid', $request->name);
        })->orderBy('created_at','desc')->paginate(20);
        foreach ($hotfaces as $key => $value){
            $listArray[] = $value->_id;
        }

        $new = [];

        if (!$request->name){
            $data = HotFaceData::whereIn('hot_face_id',$listArray)->get();
            $new = array();

            foreach ($data as $value){
                $new[$value->hot_face_id] = $value;
            }
        }


        return view('admin.hotfaces.index', compact('hotfaces','new'));
    }

    /**
     * Show the form for creating a new hotface.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.hotfaces.edit', $this->createOrEdit());
    }

    /**
     * Store a newly created hotface in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\HotFace $hotface
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, HotFace::$rules);

        $this->storeOrUpdate($request);

        flash()->success("Lưu thành công");

        return redirect()->route('admin.hotfaces.index');
    }

    /**
     * Display the specified hotface.
     *
     * @param  \App\HotFace $hotface
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(HotFace $hotface)
    {

        $data = HotFaceData::where('hot_face_id', $hotface->id)->first();
        $posts = Post::where('hot_face_id', $hotface->id)->orderBy('fb_created', 'desc')->limit(intval($hotface->number_post))->get();
        return view('admin.hotfaces.show', compact('hotface', 'posts','data'));

    }


    public function warning($id){

        $user = HotFace::find($id);
        $user->warning = '1';
        $test = $user->save();

        if($test){
            return response()->json([
                'status'  => 200,
                'message' => 'Success',
            ]);
        }

    }

    /**
     * Show the form for editing the specified hotface.
     *
     * @param  \App\HotFace $hotface
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(HotFace $hotface)
    {

        return view('admin.hotfaces.edit', $this->createOrEdit($hotface));
    }

    /**
     * Update the specified hotface in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\HotFace $hotface
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HotFace $hotface)
    {

        $rules = [
            'fb_uid' => 'required',
        ];
        $this->validate($request, $rules);
        $this->storeOrUpdate($request, $hotface);

        flash()->success('Lưu thành công');

        return back();
    }

    /**
     * Store or update a hotface.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\HotFace|null $blog
     * @return \App\Models\HotFace
     */
    protected function storeOrUpdate(Request $request, HotFace $hotface = null)
    {
        $hotface = $hotface ?: new HotFace;
        $data    = $request->all();
        $hotface->fill($data);

        $hotface->save();

        return $hotface;
    }

    /**
     * Remove the specified hotface from storage.
     *
     * @param  \App\Models\HotFace $hotface
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(HotFace $hotface)
    {

        $hotface->delete();

        if (request()->ajax()) {
            return response()->json([
                'message'  => trans('hotface.deleted'),
                'redirect' => route('admin.hotfaces.index'),
            ]);
        }

        flash()->success('Xóa thành công');

        return redirect()->route('admin.hotfaces.index');
    }

    /**
     * Get create or add view params.
     *
     * @param  \App\Models\HotFace $hotface
     * @return array
     */
    protected function createOrEdit(HotFace $hotface = null)
    {
        $hotface = $hotface ?: new HotFace;
        $url     = $hotface->id
        ? route('admin.hotfaces.update', [$hotface->id])
        : route('admin.hotfaces.store');
        return compact('hotface', 'url');
    }

    public function showAddMany()
    {
        return view('admin.hotfaces.add-many');
    }

    public function addMany(Request $request)
    {
        $number_post = $request->number_post;
        $sync_like   = $request->sync_like;
        $raw         = $request->content;
        $arr         = explode(",", $raw);
        $newArr      = [];
        foreach ($arr as $key => $item) {
            $newArr[] = trim($item);
        }
        $count = 0;
        foreach ($newArr as $key => $content) {
            if (strlen($content) > 10) {
                $hotface = HotFace::where('fb_uid', $content)->first();

                if (!$hotface) {
                    $hotface              = new HotFace();
                    $hotface->number_post = $number_post;
                    $hotface->sync_like   = $sync_like;
                    $hotface->fb_uid      = $content;
                    $hotface->save();
                    $count++;
                }
            }
        }
        flash()->success("Thêm mới thành công " . $count . " uid!");

        return redirect()->route('admin.hotfaces.index');
    }

    public function rawData(){
        $data    =  HotFaceRaw::where('hide','!=',1)->paginate(20);
        return view('admin.hotfaces.raw',compact('data'));
    }

    public function hideraw($id){

        $data = HotFaceRaw::find($id);
        $data->hide = 1;
        $data->save();



       return Redirect::back();

    }

}
