<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Token;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    /**
     * Display a listing of the token.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tokens = Token::when($request->content, function ($q) use ($request) {
            return $q->where('content', 'like', '%' . $request->content . '%');
        })->orderBy('created_at','desc')->paginate(20);
        return view('admin.tokens.index', compact('tokens'));
    }

    /**
     * Show the form for creating a new token.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.tokens.edit', $this->createOrEdit());
    }

    /**
     * Store a newly created token in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Token $token
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, Token::$rules);

        $this->storeOrUpdate($request);

        flash()->success("Lưu thành công");

        return redirect()->route('admin.tokens.index');
    }

    /**
     * Display the specified token.
     *
     * @param  \App\Token $token
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Token $token)
    {
        // $posts = Post::where('hot_face_id', $token->id)->orderBy('fb_created', 'desc')->limit(intval($token->number_post))->get();
        // return view('admin.tokens.show',compact('token','posts'));
    }

    /**
     * Show the form for editing the specified token.
     *
     * @param  \App\Token $token
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Token $token)
    {

        return view('admin.tokens.edit', $this->createOrEdit($token));
    }

    /**
     * Update the specified token in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Token $token
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Token $token)
    {

        $rules = [
            'content' => 'required',
        ];
        $this->validate($request, $rules);
        $this->storeOrUpdate($request, $token);

        flash()->success('Lưu thành công');

        return back();
    }

    /**
     * Store or update a token.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Token|null $blog
     * @return \App\Models\Token
     */
    protected function storeOrUpdate(Request $request, Token $token = null)
    {
        $token = $token ?: new Token;
        $data  = $request->all();
        $token->fill($data);

        $token->save();

        return $token;
    }

    /**
     * Remove the specified token from storage.
     *
     * @param  \App\Models\Token $token
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Token $token)
    {

        $token->delete();

        if (request()->ajax()) {
            return response()->json([
                'message'  => trans('token.deleted'),
                'redirect' => route('admin.tokens.index'),
            ]);
        }

        flash()->success('Xóa thành công');

        return redirect()->route('admin.tokens.index');
    }

    /**
     * Get create or add view params.
     *
     * @param  \App\Models\Token $token
     * @return array
     */
    protected function createOrEdit(Token $token = null)
    {
        $token = $token ?: new Token;
        $url   = $token->id
        ? route('admin.tokens.update', [$token->id])
        : route('admin.tokens.store');
        return compact('token', 'url');
    }

    public function showAddMany()
    {
        return view('admin.tokens.add-many');
    }

    public function addMany(Request $request)
    {
        $status = $request->status;
        $raw    = $request->content;
        $arr    = explode(",", $raw);
        $newArr = [];
        foreach ($arr as $key => $item) {
            $newArr[] = trim($item);
        }
        $count = 0;
        foreach ($newArr as $key => $content) {
            if (strlen($content) > 10) {
                $token = Token::where('content', $content)->first();

                if (!$token) {
                    $token          = new Token();
                    $token->status  = $status;
                    $token->content = $content;
                    $token->save();
                    $count++;
                }
            }
        }
        flash()->success("Thêm mới thành công " . $count . " token!");

        return redirect()->route('admin.tokens.index');
    }
}
