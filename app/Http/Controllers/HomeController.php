<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Models\FbLocation;
use App\Models\PostReactionFbUser;
use App\Models\HotFace;
use App\Models\Post;
use App\Models\Token;

class HomeController extends Controller
{
    public function __construct()
    {
        //$this->access_token = env('FB_ACCESS_TOKEN');
        $this->access_token = 'EAAAAUaZA8jlABAESKrg631CGBEH9LgKjEIs5zhjWeqHnkikc4VlH9xRvj3g49ZAuZAznqVfn0devsBpIgm0iIcPmTBKVnbk2Iwrf9WiPqcOloPZC9UcdSTepZCidXD76Qg3bffKRDbODTogYytjHyL8qq5iijOkKTqULsEFlMYJfCzwHK2yL53ljI0r0TkY8ZD';
    }

    /**
     * @var \GuzzleHttp\Client
     */
    protected static $httpClient;

    protected $access_token;

    protected $data = [];

    protected $next_url = '';

    /**
     * Get guzzle http client.
     *
     * @return \GuzzleHttp\Client
     */
    protected function httpClient()
    {
        if (!static::$httpClient) {
            static::$httpClient = new Client([
                'base_uri' => 'https://graph.facebook.com',
            ]);
        }

        return static::$httpClient;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function test(Request $request)
    {
        //$this->access_token = TokenHelper::getToken();
            $response = $this->httpClient()->post('100002980102535/subscribers', [
                'query' => [
                    'access_token' => $this->access_token,
                ],
            ]);
            $body          = $response->getBody();
            $data          = json_decode($body->getContents());
            dd($data);
        
    }

    protected function stripUnicode($str)
    {
        $str = trim(mb_strtolower($str));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', ' ', $str);
        return $str;
    }

    protected function replaceSpace($str){
        $str = preg_replace('/([ ]+)/', '', $str);
        return $str;
    }

    protected function getLocationProvince($item, $postIds) {
        $locationIds = FbLocation::where('name_unicode', 'regexp', '/'.$item['key1'].'/i')->orWhere('name_unicode', 'regexp', '/'.$item['key2'].'/i')->pluck('fb_id');
        $count    = PostReactionFbUser::distinct('fb_user_id')->where('update_time',1)->whereIn('post_id', $postIds)
            ->whereIn('location_id', $locationIds)
            ->count();
        return $count;
    }

}
