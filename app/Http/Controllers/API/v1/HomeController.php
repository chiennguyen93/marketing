<?php

namespace App\Http\Controllers\API\v1;

use App\Models\FbUser;
use App\Models\FbUsers;
use App\Models\HotFace;
use App\Models\HotFaceRaw;
use App\Models\Post;
use App\Models\HotFaceData;
use Illuminate\Http\Request;


use App\Support\TokenHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

use Cache;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Admin\ProcessingUsersController;
class HomeController
{

    protected $access_token;
    protected $processingController;

    public function __construct(ProcessingUsersController $processingUsersController)
    {
        $this->access_token = TokenHelper::getToken();
        $this->processingController = $processingUsersController;
    }

    public function fanStatistics(Request $request)
    {
        $hotFace = HotFace::where('fb_uid', $request->uid)->first();
        if (!$hotFace) {
            return response()->json([
                'status'  => 404,
                'message' => 'Không tìm thấy Uid',
                'data'    => null,
            ]);
        } else {
            $hotFaceData = HotFaceData::where('hot_face_id', $hotFace->id)->first();
            $location = array();
            $age_range = array();

            foreach ($hotFaceData->location as $key => $value){
                $location[$key] =  round($value, 2);
            }

            foreach ($hotFaceData->age_range as $key => $value){
                $age_range[$key] =  round($value, 2);
            }




            $hotFaceData->location = $location;
            $hotFaceData->age_range = $age_range;

            if (!$hotFaceData) {
                return response()->json([
                    'status'  => 404,
                    'message' => 'Không tìm thấy Uid',
                    'data'    => null,
                ]);
            } else {
                return response()->json([
                    'status'  => 200,
                    'message' => 'Success',
                    'data'    => $hotFaceData->makeHidden(['_id', 'hot_face_id', 'updated_at', 'created_at']),
                ]);
            }
        }
    }

    public function post(Request $request)
    {
        $post = Post::where('fb_id', $request->postId)->first();
        if (!$post) {
            return response()->json([
                'status'  => 404,
                'message' => 'Không tìm thấy Post',
                'data'    => null,
            ]);
        } else {
            $post->total_comment = $post->count_comments;
            $post->total_share = $post->count_shares;
            // $post->total_like = $post->count_likes + $post->count_loves + $post->count_wows + $post->count_hahas + $post->count_sads + $post->count_angrys + $post->count_thankfuls;
            $post->makeHidden(['_id', 'hot_face_id','fb_uid','count_comments', 'count_shares','count_likes','count_loves','count_wows','count_hahas','count_sads','count_angrys','count_thankfuls']);
            return response()->json([
                'status'  => 200,
                'message' => 'Success',
                'data'    => $post,
            ]);
        }
    }

    public function getall(){

        for ($i = 0 ; $i < 100; $i++){
            if(Cache::has('Uid-cache')){
                $id = Cache::get('Uid-cache');

                $dataUser = FbUsers::where('_id','=', $id)->firstOrFail();


                $sub =  $this->getSubscribers($dataUser->fb_uid);


                if ($sub > 5000 ){
                    $this->saveData($sub,$dataUser,$i);
                    $nextID = FbUser::where('_id', '>', $id)->firstOrFail();
                    $nextID = $nextID->_id;
                }elseif($sub === 'error'){
                    $nextID = $id;
                }elseif($sub < 5000 ){
                    $nextID = FbUser::where('_id', '>', $id)->firstOrFail();
                    $nextID = $nextID->_id;
                }


                Cache::forever('Uid-cache', $nextID);

            }else{
                Cache::forever('Uid-cache','5b3032436cfe995a7a0527e2');
            }
        }

    }

    public function getPost(Request $request){
        try {
            return $this->processingController->exportAPI($request->postId);
        } catch (ClientException $e) {
            $this->access_token = TokenHelper::changeToken();
            \Log::debug($e->getMessage());
            return 0 ;
        }
    }

    protected function getSubscribers($uid)
    {
        try {
            $response = $this->httpClient()->get('https://graph.facebook.com/v1.0/'. $uid .'/subscribers', [
                'query' => [
                    'limit'        => 1,
                    'access_token' => $this->access_token,
                ],
            ]);
            $body = $response->getBody();
            $data = json_decode($body->getContents());

            if (isset($data->summary)) {
                if (isset($data->summary->total_count)) {
                    return $data->summary->total_count ;
                }
            }
            return 0;
        } catch (ClientException $e) {
            $this->access_token = TokenHelper::changeToken();
            \Log::debug($e->getMessage());
            return 0 ;
        }
    }

    protected function httpClient()
    {
        $httpClient = new Client([
            'base_uri' => 'https://graph.facebook.com/v3.0',
        ]);

        return $httpClient;
    }

    protected function saveData($sub,$dataUser){
        $exists = HotFace::where('fb_uid', '=', $dataUser->fb_uid)->exists();
        $existsRaw = HotFaceRaw::where('fb_uid', '=', $dataUser->fb_uid)->exists();

        if(!$exists && !$existsRaw){
            $saveData = new HotFaceRaw;
            $saveData->count_followers = $sub;
            $saveData->fb_uid = $dataUser->fb_uid;
            $saveData->gender =  $dataUser->gender;
            $saveData->location_id =  $dataUser->location_id;
            $saveData->save();
        }
    }

}