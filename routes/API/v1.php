<?php


Route::prefix('auth')
    ->as('auth.')
    ->group(function () {
        Route::post('/', 'Auth\LoginController@login')
            ->name('login');

        Route::post('logout', 'Auth\LoginController@logout')
            ->name('logout');

    });

//Route::middleware('auth:api')
//    ->group(function () {
        Route::get('uid/fan-statistics/{uid}','HomeController@fanStatistics');
    	Route::get('uid/posts/{postId}','HomeController@post');
        Route::get('test','HomeController@getall');
        Route::get('get-post/{postId}','HomeController@getPost');
//    });


// Route::group([

//     'middleware' => 'api'
    

// ], function ($router) {

//     Route::get('uid-data/{uid}','HomeController@data');

// });