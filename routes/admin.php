<?php

Route::get('login', 'Auth\Admin\LoginController@showLoginForm')
    ->name('login.form');

Route::post('login', 'Auth\Admin\LoginController@login')
    ->name('login');

Route::get('logout', 'Auth\Admin\LoginController@logout')
    ->name('logout');
    
Route::get('register', 'Auth\Admin\RegisterController@showRegistrationForm')
    ->name('register.form');

Route::post('register', 'Auth\Admin\RegisterController@register')
    ->name('register');


Route::get('test', 'Admin\ProcessingUsersController@GetInfoPost')
    ->name('test');

Route::get('hotface', 'Admin\ProcessCronJobDataController@processIF')
    ->name('hotface');

Route::namespace ('Admin')
    ->middleware('auth:admin')
    ->group(function () {
        Route::get('/', 'HotFaceController@index')
            ->name('index');

        Route::get('tokens/add-many', 'TokenController@showAddMany')
        ->name('tokens.showAddMany');
        Route::post('tokens/add-many', 'TokenController@addMany')
        ->name('tokens.addMany');

        Route::get('hotfaces/add-many', 'HotFaceController@showAddMany')
        ->name('hotfaces.showAddMany');
        Route::post('hotfaces/add-many', 'HotFaceController@addMany')
        ->name('hotfaces.addMany');

        Route::get('hotfaces/rawdata', 'HotFaceController@rawData')
            ->name('hotfaces.raw');

        Route::get('hotfaces/warning/{id}', 'HotFaceController@warning')
            ->name('hotfaces.warning');


        Route::get('hotfaces/rawdata/delete/{id}', 'HotFaceController@hideraw')
            ->name('hotfaces.raw.hide');

        Route::resources([
            'hotfaces'  => 'HotFaceController',
            'tokens'  => 'TokenController'
        ]);

    });
