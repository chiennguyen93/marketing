<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::any('v1', [
//     'as' => 'api.v1.',
//     'namespace' => 'API\v1',
// ], function () {
//     require base_path('routes/API/v1.php');
// });

Route::prefix('v1')
    ->as('api.v1.')
    ->namespace ('API\v1')
    ->group(function () {
        require base_path('routes/API/v1.php');
    });
