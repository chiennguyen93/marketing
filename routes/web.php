<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('test','HomeController@test')->name('test');

Route::prefix('admin')
    ->as('admin.')
    ->group(function () {
        require base_path('routes/admin.php');
    });
