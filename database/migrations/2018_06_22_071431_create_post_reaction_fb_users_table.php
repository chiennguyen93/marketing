<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Jenssegers\Mongodb\Schema\Blueprint as Collection;

class CreatePostReactionFbUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mongodb')->create('post_reaction_fb_users', function (Collection $collection) {
            $collection->index('post_id');
            $collection->index('post_fb_id');
            $collection->index('hot_face_id');
            $collection->index('fb_user_uid');
            $collection->index('fb_user_id');
            $collection->index('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mongodb')->dropIfExists('post_reaction_fb_users');
    }
}
