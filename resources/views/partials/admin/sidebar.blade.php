<div class="sidebar" data-color="black" style="background-image: url( {{ asset('images/admin/full-screen-image-3.jpg') }} ) ">
    <div class="logo">
        <a href="#" class="logo-text">
            Admin
        </a>
    </div>
    <div class="logo logo-mini">
        <a href="#" class="logo-text">
            Admin
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{ asset('images/admin/default-avatar.png') }}" />
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                    {{-- {{ user()->name }} --}}Admin
                </a>
            </div>
        </div>
        <ul class="nav">
            <li class={{ active_route('admin.hotfaces.*') }}>
                <a href="{{ route('admin.hotfaces.index') }}">
                    <i class="fa fa-user-circle" aria-hidden="true"></i>
                    <p>Danh sách Uid</p>
                </a>

            </li>
            <li>
                <a href="{{ route('admin.hotfaces.raw') }}">
                    <i class="fa fa-user-circle" aria-hidden="true"></i>
                    <p>List raw</p>
                </a>
            </li>
            <li class={{ active_route('admin.tokens.*') }}>
                <a href="{{ route('admin.tokens.index') }}">
                    <i class="fa fa-qrcode" aria-hidden="true"></i>
                    <p>Danh sách token</p>
                </a>
            </li>

            {{-- <li class={{ active_route('admin.emails.*') }}>
                <a href="{{ route('admin.emails.index') }}">
                    <i class="pe-7s-mail"></i>
                    <p>Email nhận thông báo</p>
                </a>
            </li> --}}
        </ul>
    </div>
</div>