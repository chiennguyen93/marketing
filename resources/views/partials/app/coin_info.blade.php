@php
	$filtered = $blocks->filter(function ($value, $key) use( $start_date, $end_date) {
	    return $value->created_at->between($start_date, $end_date);
	});
@endphp
<p>Số giao dịch: {{ $filtered->sum('transactions') }}</p>
<p>Số coin giao dịch: {{ $filtered->sum('value') }}</p>
