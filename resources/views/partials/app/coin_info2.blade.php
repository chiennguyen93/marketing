@php
	$filtered = $blocks->filter(function ($value, $key) use( $start_date, $end_date) {
	    return $value->created_at->between($start_date, $end_date);
	});
@endphp
<tr>
	<td>
		<p>{{ $start_date->format('H:m  d/m/y ') }} -> {{ $end_date->format('H:m  d/m/y ') }}</p>
	</td>
	<td>{{ $filtered->sum('transactions') }} lần</td>
	<td>{{ $filtered->sum('value') }} coin</td>
</tr>