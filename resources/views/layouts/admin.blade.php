<!doctype html>
<html lang="@yield('htmlLang', app()->getLocale())" class="@yield('htmlClass')">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Admin</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    {!! Html::style(mix('css/vendor.css')) !!}
    {!! Html::style(mix('css/admin.css')) !!}

    
</head>

<body>
    <div class="wrapper">
        @include('partials.admin.sidebar')
        <div class="main-panel">
            @include('partials.admin.navbar')
            @yield('content')
        </div>
    </div>
</body>
    {!! Html::script(mix('js/manifest.js')) !!}
    {!! Html::script(mix('js/vendor.js')) !!}
    {!! Html::script('https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=l9i76nao5iygw33yqw0wbfr8g9qljofw54a4novb8dto24cj') !!}
    {!! Html::script(mix('js/admin.js')) !!}
</html>