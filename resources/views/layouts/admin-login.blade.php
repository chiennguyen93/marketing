<!doctype html>
<html lang="@yield('htmlLang', app()->getLocale())" class="@yield('htmlClass')">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Admin</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! Html::style(mix('css/vendor.css')) !!}
    {!! Html::style(mix('css/admin.css')) !!}
</head>

<body>
<nav class="navbar navbar-transparent navbar-absolute">
    <div class="container">    
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="">Marketing</a>
        </div>
        <div class="collapse navbar-collapse">       
            
            <ul class="nav navbar-nav navbar-right">
                <li>
                    @if (Request::route()->getName() == "admin.register.form")
                        <a href="{{ route('admin.login.form') }}">
                            Login
                        </a>
                    @else
                        <a href="{{ route('admin.register.form') }}">
                            Register
                        </a>
                    @endif
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="wrapper wrapper-full-page">
    <div class="full-page login-page"  data-color="black"   style="background-image: url( {{ asset('images/admin/full-screen-image-3.jpg') }} ) ">   
        
        {{-- <div class="content">
            <div class="container">
                <div class="row">                   
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <form method="#" action="#">
                            
                            <div class="card">
                                <div class="header text-center">Login</div>
                                <div class="content">
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <input type="email" placeholder="Enter email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" placeholder="Password" class="form-control">
                                    </div>                                    
                                    <div class="form-group">
                                        <label class="checkbox">
                                            <input type="checkbox" data-toggle="checkbox" value="">
                                            Subscribe to newsletter
                                        </label>    
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <button type="submit" class="btn btn-fill btn-warning btn-wd">Login</button>
                                </div>
                            </div>
                                
                        </form>
                                
                    </div>                    
                </div>
            </div>
        </div> --}}
        @yield('body')
        <footer class="footer footer-transparent">
            <div class="container">
                <p class="copyright pull-right">Chiennd
                </p>
            </div>
        </footer>

    </div>                             
       
</div>
</body>
    {!! Html::script(mix('js/manifest.js')) !!}
    {!! Html::script(mix('js/vendor.js')) !!}
    {!! Html::script('https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=l9i76nao5iygw33yqw0wbfr8g9qljofw54a4novb8dto24cj') !!}
    {!! Html::script(mix('js/admin.js')) !!}
</html>