
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="">

    <title>Blockchain</title>
    
    {!! Html::style(mix('css/app.css')) !!}
    
</head>

<body>

    <header>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Blockchain</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="{{ route('admin.index') }}"><span class="glyphicon glyphicon-user"></span> Admin</a></li>
                    <li><a href="{{ route('admin.logout') }}"><span class="glyphicon glyphicon-log-in"></span> Đăng xuất</a></li>
                </ul>
              </div>
            </div>
        </nav>
    </header>

<main role="main" id="app">
    @yield('content')
</main>

{{-- <footer class="text-muted">
  <div class="container">
    <p class="float-right">
      <a href="#">Back to top</a>
  </p>
  <p>Album example is &copy; Bootstrap, but please download and customize it for yourself!</p>
  <p>New to Bootstrap? <a href="../../">Visit the homepage</a> or read our <a href="../../getting-started/">getting started guide</a>.</p>
</div>
</footer> --}}


{!! Html::script(mix('js/manifest.js')) !!}
{!! Html::script(mix('js/vendor.js')) !!}
{!! Html::script(mix('js/app.js')) !!}
</body>
</html>
