@extends('layouts.app')
@section('content')
<div class="container" style="margin-top:100px;">
    <div class="row">
        <div class="col-md-12 text-right">
            {!! Form::open([
                'route' => 'index',
                'method' => 'get',
                'role' => 'form',
                'class' => "form-horizontal col-sm-6 col-sm-offset-6"
                ]) !!}
                <div class="input-group">
                    <input type="text" class="form-control" id="searchField" name="name" placeholder="Tên coin ..." value="{{ request('name') }}">
                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
                    </div>
                </div>
                {!! Form::close() !!}
        </div>
        <br/>
        <br/>
        <br/>
        <div class="col-md-12">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Tên Coin</th>
                        <th>Biểu đồ giao dịch 15 ngày</th>
                        <th>Biểu đồ số lượng 15 ngày</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 1;
                    @endphp
                    @foreach ($coins as $coin)
                        <tr>
                            <td>{{ $i++ }}</td>
                            <td> 
                                <a href="{{ route('detail', $coin->code) }}">{{ $coin->name }}</a>
                            </td>
                            <td>
                                @php
                                    $dataObject = new stdClass();
                                    $labels = [];
                                    $transactions = [];
                                    foreach ($coin->coinInfor as $key => $coinInfor) {
                                        array_push($labels, $coinInfor->date->format('H:s d-m'));
                                        array_push($transactions, $coinInfor->transactions);
                                    }
                                    $datasets1Object = new stdClass();
                                    $datasets1Object->label = 'Số giao dịch';
                                    $datasets1Object->backgroundColor = 'transparent';
                                    $datasets1Object->borderColor = '#6888f9';
                                    $datasets1Object->pointBackgroundColor = '#021f84';
                                    $datasets1Object->data = $transactions;

                                    $datasets1 = [];
                                    array_push($datasets1, $datasets1Object);

                                    $dataObject->labels = $labels;
                                    $dataObject->datasets = $datasets1;
                                    //dd(json_encode($dataObject));
                                @endphp

                                <seven-chart
                                :width="500"
                                :height="250"
                                :data="{{ json_encode($dataObject) }}"
                                ></seven-chart>
                            </td>
                            <td>
                                @php
                                    $dataObject = new stdClass();
                                    $labels = [];
                                    $value = [];
                                    foreach ($coin->coinInfor as $key => $coinInfor) {
                                        array_push($labels, $coinInfor->date->format('H:s d-m'));
                                        array_push($value, $coinInfor->value);
                                    }
                                    $datasets1Object = new stdClass();
                                    $datasets1Object->label = 'Số lượng coin';
                                    $datasets1Object->backgroundColor = 'transparent';
                                    $datasets1Object->borderColor = '#6888f9';
                                    $datasets1Object->pointBackgroundColor = '#021f84';
                                    $datasets1Object->data = $value;

                                    $datasets1 = [];
                                    array_push($datasets1, $datasets1Object);

                                    $dataObject->labels = $labels;
                                    $dataObject->datasets = $datasets1;
                                    //dd(json_encode($dataObject));
                                @endphp

                                <seven-chart
                                :width="500"
                                :height="250"
                                :data="{{ json_encode($dataObject) }}"
                                ></seven-chart>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="text-right">
                {{ $coins->appends(request()->query())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
