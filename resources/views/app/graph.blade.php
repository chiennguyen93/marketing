@extends('layouts.app')
@section('content')
<div class="container" style="margin-top:100px;">
    <div class="row">
        <div class="col-md-2">
            <h3>{{ $coin->name }}</h3>
        </div>
        {{-- <div class="col-md-2">
            <p>Số giao dịch: {{ $header_report->sum('transactions') }}</p>
            <p>Số coin: {{ $header_report->sum('value') }}</p>
        </div> --}}
        <form method="GET" action="{{ route('graph', $coin->code) }}">
            <div class="col-md-2">
                <input type="search" name="start_date"  class="datetimepicker form-control" value="{{ request('start_date') }}" placeholder="Ngày bắt đầu">
            </div>
            <div class="col-md-2">
                <input type="search" name="end_date"  class="datetimepicker form-control" value="{{ request('end_date') }}" placeholder="Ngày kết thúc">
            </div>
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary">Xem biểu đồ</button>
            </div>
            <div class="col-md-2">
                <a type="submit" class="btn btn-warning" href="{{ route('detail', $coin->code) }}">Quay lại</a>
            </div>
        </form>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12" style="background: #f5f3f361; margin-bottom: 100px;">
            <h4>Biểu đồ lượng giao dịch</h4><br>
            @php
                $dataObject = new stdClass();
                $labels = [];
                $transactions = [];
                foreach ($coinInfors as $key => $coinInfor) {
                    array_push($labels, $coinInfor->date->format('H:s d-m'));
                    array_push($transactions, $coinInfor->transactions);
                }
                $datasets1Object = new stdClass();
                $datasets1Object->label = 'Số giao dịch';
                $datasets1Object->backgroundColor = 'transparent';
                $datasets1Object->borderColor = '#6888f9';
                $datasets1Object->pointBackgroundColor = '#021f84';
                $datasets1Object->data = $transactions;

                $datasets1 = [];
                array_push($datasets1, $datasets1Object);

                $dataObject->labels = $labels;
                $dataObject->datasets = $datasets1;
                //dd(json_encode($dataObject));
            @endphp

            <graph-chart
            :width="1000"
            :height="500"
            :data="{{ json_encode($dataObject) }}"
            ></graph-chart>
        </div>
        <div class="col-md-12" style="background: #f5f3f361;">
            <h4>Biểu đồ lượng coin</h4><br>
            @php
                $dataObject = new stdClass();
                $labels = [];
                $value = [];
                foreach ($coinInfors as $key => $coinInfor) {
                    array_push($labels, $coinInfor->date->format('H:s d-m'));
                    array_push($value, $coinInfor->value);
                }
                $datasets1Object = new stdClass();
                $datasets1Object->label = 'Số giao dịch';
                $datasets1Object->backgroundColor = 'transparent';
                $datasets1Object->borderColor = '#6888f9';
                $datasets1Object->pointBackgroundColor = '#021f84';
                $datasets1Object->data = $value;

                $datasets1 = [];
                array_push($datasets1, $datasets1Object);

                $dataObject->labels = $labels;
                $dataObject->datasets = $datasets1;
                //dd(json_encode($dataObject));
            @endphp

            <graph-chart
            :width="1000"
            :height="500"
            :data="{{ json_encode($dataObject) }}"
            ></graph-chart>
        </div>
    </div>
    <div style="margin-bottom: 200px"></div>
</div>
@endsection
