@extends('layouts.app')
@section('content')
<div class="container" style="margin-top:100px;">
    <div class="row">
        <div class="col-md-2">
            <h3>{{ $coin->name }}</h3>
        </div>
        {{-- <div class="col-md-2">
            <p>Số giao dịch: {{ $header_report->sum('transactions') }}</p>
            <p>Số coin: {{ $header_report->sum('value') }}</p>
        </div> --}}
        <form method="GET" action="{{ route('graph', $coin->code) }}">
            <div class="col-md-2">
                <input type="search" name="start_date"  class="datetimepicker form-control" value="{{ request('start_date') }}" placeholder="Ngày bắt đầu">
            </div>
            <div class="col-md-2">
                <input type="search" name="end_date"  class="datetimepicker form-control" value="{{ request('end_date') }}" placeholder="Ngày kết thúc">
            </div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary">Xem biểu đồ</button>
            </div>
        </form>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th>Height</th>
                        <th>Time</th>
                        <th>Tổng giao dịch</th>
                        <th>Tổng lượng coin</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $n = 1;
                    @endphp
                    @foreach ($blocks as $block)
                        <tr>
                            <td>{{ $n++ }}</td>
                            <td>{{ $block->height }}</td>
                            @php
                                //dd($block->time);
                            @endphp
                            <td>{{ $block->time->format('H:i:s  d/m/Y') }}</td>
                            <td>{{ $block->transactions }}</td>
                            <td>{{ $block->value }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="text-right">
                {{ $blocks->appends(request()->query())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
