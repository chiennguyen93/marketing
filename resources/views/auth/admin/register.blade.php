@extends('layouts.admin-login')

@section('bodyClass', 'appWrapper')

@section('body')
<div class="content">
    <div class="container">
        <div class="row">                   
            <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                <form method="POST" action="{{ route('admin.register') }}">
                    <div class="card">
                        <div class="header text-center">Register</div>
                        <div class="content">
                            <div class="form-group">
                                <label>Email address</label>
                                <input type="email" name="email" placeholder="Enter email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" name="name" placeholder="Enter name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" name="password" placeholder="Password" class="form-control">
                            </div> 
                            {!! Form::token() !!}
                        </div>
                        <div class="footer text-center">
                            <button type="submit" class="btn btn-fill btn-warning btn-wd">Register</button>
                        </div>
                    </div>
                        
                </form>
                        
            </div>                    
        </div>
    </div>
</div>
@endsection
