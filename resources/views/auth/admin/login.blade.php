@extends('layouts.admin-login')

@section('bodyClass', 'appWrapper')

@section('body')
<div class="content">
    <div class="container">
        <div class="row">                   
            <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                <form method="POST" action="{{ route('admin.login') }}">
                    <div class="card">
                        <div class="header text-center">Login</div>
                        <div class="content">
                            <div class="form-group">
                                <label>Email address</label>
                                <input type="email" placeholder="Enter email" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" placeholder="Password" name="password" class="form-control">
                            </div> 
                            {!! Form::token() !!}
                            @include('partials.admin.errors')
                        </div>
                        <div class="footer text-center">
                            <button type="submit" class="btn btn-fill btn-warning btn-wd">Login</button>
                        </div>
                    </div>
                        
                </form>
                        
            </div>                    
        </div>
    </div>
</div>
@endsection
