@extends('layouts.admin')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header"> <h4>List data raw</h4></div>
                        <div class="content">
<!--                            --><?php //var_dump($data); ?>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Follower</th>
                                        <th>FB Link</th>
                                        <th>Gender</th>
                                        <th>hide</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = (request('page', 1) - 1) * 20; ?>
                                    @foreach($data as $key => $value)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $value->count_followers }}</td>
                                            <td><a target="_blank" href="{{'https://www.facebook.com/'. $value->fb_uid}}">Link</a></td>
                                            <td>{{ $value->gender  }}</td>
                                            <td>
                                                <a rel="tooltip" title="" class="btn btn-simple btn-danger btn-icon table-action remove"
                                                   href="{{route('admin.hotfaces.raw.hide', ['id' => $value->_id])}}"
                                                    aria-describedby="tooltip791706">
                                                    <i class="fa fa-remove"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            {{--{{ $data->links() }}--}}
                        </div>
                    </div>
                    <div class="fixed-table-pagination">
                        <div class="pull-right pagination">
                            {{ $data->appends(request()->query())->links() }}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection