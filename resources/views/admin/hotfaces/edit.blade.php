@extends('layouts.admin')


@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card" >
                    <div class="header">
                        <legend>{{ $hotface->id ? 'Sửa hotface' : 'Thêm mới' }}</legend>
                    </div>
                    <div class="content">
                        @include('partials.admin.errors')
                        @include('flash::message')
                    </div>
                    <div class="content">
                    	{!! Form::model($hotface, [
		                    'url' => $url,
		                    'role' => 'form',
		                    'method' => $hotface->id ? 'PUT' : 'POST',
		                    'files' => true,
		                ]) !!}
    		                <div class="row">
                            	<div class="col-md-6">
        		                    	<div class="form-group">
                                        	{!! Form::label('fb_uid', 'Uid',[
                                        			'for' => 'fb_uid',
                                        		]) !!}
        		                            {!! Form::text('fb_uid', old('fb_uid'), [
        		                                'placeholder' => 'Uid',
        		                                'class' => 'form-control'
        		                            ]) !!}
                                        </div>
                                    <div class="form-group">
                                    	{!! Form::label('number_post', 'Số lượng bài viết muốn lấy',[
                                    			'for' => 'number_post'
                                    		]) !!}
    		                            {!! Form::number('number_post', $hotface->number_post ?: 50, [
    		                                'placeholder' => 'Số lượng bài viết muốn lấy',
    		                                'class' => 'form-control'
    		                            ]) !!}
                                    </div>
                            	</div>
                        		<div class="col-md-6">
                        			<div class="form-group">
                            			{!! Form::label('sync_like', 'Quét danh sách like',[
                                    			'for' => 'sync_like'
                                    		]) !!}
    					                {!! Form::select('sync_like', ['1' => 'Có', '0' => 'Không'], request('sync_like',1), [
    					                    'class' => 'form-control',
    					                    'data-toggle' => 'type',
    					                ]) !!}
    					            </div>
    				            </div>
    				            <div class="col-md-6">
    				            	<div class="row text-center">
    			                        <div class="form-group">
    			                            {!! Form::submit('Lưu', [
    			                                'class' => 'btn btn-primary btn-fill',
    			                            ]) !!}
    			                        </div>
    		                    	</div>
                            	</div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection