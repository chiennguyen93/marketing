@extends('layouts.admin')


@section('content')
    <div class="content">



        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card" >
                        <div class="header">
                            <legend>Chi tiết Uid</legend>
                        </div>
                        <div class="content">
                            @include('partials.admin.errors')
                            @include('flash::message')
                        </div>
                        <div class="content">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">

                                        {!! Form::label('fb_uid', 'Uid',[
                                           'for' => 'fb_uid',
                                        ]) !!}
                                        {!! Form::text('fb_uid', $hotface->fb_uid, [
                                          'placeholder' => 'Uid',
                                          'disabled' => 'disabled',
                                          'class' => 'form-control'
                                        ]) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('number_post', 'Số lượng bài viết muốn lấy',[
                                            'for' => 'number_post'
                                        ]) !!}
                                        {!! Form::number('number_post', $hotface->number_post ?: 50, [
                                            'placeholder' => 'Số lượng bài viết muốn lấy',
                                            'disabled' => 'disabled',
                                            'class' => 'form-control'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! Form::label('sync_like', 'Quét danh sách like',[
                                           'for' => 'sync_like'
                                        ]) !!}
                                        {!! Form::select('sync_like', ['1' => 'Có', '0' => 'Không'], request('sync_like',1), [
                                            'class' => 'form-control',
                                            'disabled' => 'disabled',
                                            'data-toggle' => 'type',
                                        ]) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('count_followers', 'Số theo dõi',[
                                            'for' => 'count_followers'
                                        ]) !!}
                                        {!! Form::number('count_followers', $hotface->count_followers, [
                                            'placeholder' => 'Số theo dõi',
                                            'disabled' => 'disabled',
                                            'class' => 'form-control'
                                        ]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>


                        @if( $data)
                            <div class="container">
                                <H2>GENDER</H2>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Gioi tinh</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data->gender as $key => $value)
                                        <tr>
                                            <td>{{ $key }} </td>
                                            <td>{{ $value }} %</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <H2>AGE RANGE</H2>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Age Range</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data->age_range as $key => $value)
                                        <tr>
                                            <td>{{ $key }} </td>
                                            <td>{{ $value }} %</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <H2>location</H2>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Age Range</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data->location as $key => $value)
                                        <tr>
                                            <td>{{ $key }} </td>
                                            <td>{{ $value }} %</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <H2>location</H2>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Key</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>count_followers</td>
                                        <td>{{ $data->count_followers }}</td>
                                    </tr>
                                    <tr>
                                        <td>like_per_post</td>
                                        <td>{{ $data->like_per_post }}</td>
                                    </tr>
                                    <tr>
                                        <td>comment_per_post</td>
                                        <td>{{ $data->comment_per_post }}</td>
                                    </tr>
                                    <tr>
                                        <td>share_per_post</td>
                                        <td>{{ $data->share_per_post }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            @endif
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="card" >
                        <div class="header">
                            <legend>Danh sách post</legend>
                        </div>
                        <table class="table">
                            <thead>
                            <th>#</th>
                            <th>Type</th>
                            <th>Nội dung</th>
                            <th>Like/Comment/Share</th>
                            <th>Đính kèm</th>
                            </thead>
                            <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($posts as $post)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>
                                        {{ $post->type }}
                                    </td>
                                    <td>
                                        <a href="{{ 'https://facebook.com/'.$post->fb_id }}">{{ str_limit($post->message,100) }}</a>
                                    </td>
                                    <td>
                                        {{ $post->totalLike.'/'.$post->count_comments.'/'.$post->count_shares }}
                                    </td>
                                    <td>
                                        {{ count($post->attachments) }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection

