@extends('layouts.admin')

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Danh sách Uid</h4>
                    </div>
                    <div class="content">
                        @include('partials.admin.errors')
                        @include('flash::message')
                    </div>
                    <div class="fixed-table-toolbar">
                        <div class="columns columns-right pull-right">
                            <a href="{{ route('admin.hotfaces.create') }}" class="btn btn-default" type="button" name="refresh" title="Refresh"><i class="glyphicon fa fa-plus"></i>Thêm mới</a>
                            <a href="{{ route('admin.hotfaces.addMany') }}" class="btn btn-default" type="button" name="refresh" title="Refresh"><i class="glyphicon fa fa-plus"></i>Thêm nhiều</a>
                        </div>
                        <div class="bars pull-right">
                            <div class="toolbar ">
                                <form class="form-inline" method="GET" action="{{ route('admin.hotfaces.index') }}">
                                    <input class="form-control" type="text" name="name" placeholder="Tên, Uid ..." value="{{ request('name') }}">
                                    <button type="submit" class="btn btn-info">Tìm</button>
                                </form>
                            </div>
                        </div>
                        {{-- <div class="columns columns-right pull-right">
                            <a href="{{ route('admin.hotfaces.create') }}" class="btn btn-default" type="button" name="refresh" title="Refresh"><i class="glyphicon fa fa-plus"></i>Thêm mới</a>
                        </div> --}}
                    </div>
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>Status</th>
                            <th>Uid</th>
                            <th>Tên</th>
                            <th>Ngày thêm</th>
                            <th>Hành động</th>
                        </thead>
                        <tbody>
                            @php
                                $i = (request('page', 1) - 1) * 20;
                            @endphp
                            @foreach ($hotfaces as $hotface)
                                <tr>
                                    <td>{{ ++$i }}
                                        @if( isset($new[$hotface->_id]) && $new[$hotface->_id]->like_per_post < 300 && $new[$hotface->_id]->comment_per_post < 20)
                                            <a class="text-warning" style="font-size: 15px">
                                                <i class="glyphicon glyphicon-warning-sign"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($hotface->trashed())
                                            <span class="label label-danger">Not found</span>
                                        @else
                                            <span class="label label-success">Acvite</span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.hotfaces.show',$hotface->id) }}">{{ $hotface->fb_uid }}</a>
                                    </td>
                                    <td>{{ $hotface->name }}</td>
                                    <td>{{ $hotface->created_at->format('H:i d/m/y') }}</td>
                                    <td class="td-actions" style="">
                                        <a rel="tooltip" title="" class="btn btn-simple btn-warning btn-icon table-action edit" href="{{ route('admin.hotfaces.edit', $hotface->_id) }}" data-original-title="Chỉnh sửa"><i class="fa fa-edit"></i></a>

                                        <a rel="tooltip" title="" class="btn btn-simple btn-danger btn-icon table-action remove" href="{{ route('admin.hotfaces.destroy', $hotface->id) }}" data-original-title="Xóa" data-swal="{{ json_encode(swal([
                                                    'title' => 'Are you sure',
                                                    'type' => 'question',
                                                    'showCancelButton' => true,
                                                ])) }}"
                                                data-swal-method="DELETE"><i class="fa fa-remove"></i></a>



                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="fixed-table-pagination">
                        <div class="pull-right pagination">
                            {{ $hotfaces->appends(request()->query())->links() }}
                        </div>
                    </div>
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

@endsection