@extends('layouts.admin')


@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card" >
                    <div class="header">
                        <legend>{{ $user->id ? 'Sửa Người dùng' : 'Thêm mới' }}</legend>
                    </div>
                    @include('partials.admin.errors')
            		@include('flash::message')
                    <div class="content">
                    	{!! Form::model($user, [
		                    'url' => $url,
		                    'role' => 'form',
		                    'method' => $user->id ? 'PUT' : 'POST',
		                    'files' => true,
		                ]) !!}
		                <div class="row">
                        	<div class="col-md-6">
		                    	<div class="form-group">
                                	{!! Form::label('name', 'Tên',[
                                			'for' => 'name'
                                		]) !!}
		                            {!! Form::text('name', old('name'), [
		                                'placeholder' => 'Tên',
		                                'class' => 'form-control'
		                            ]) !!}
                                </div>
                                <div class="form-group">
                                	{!! Form::label('email', 'Email',[
                                			'for' => 'email'
                                		]) !!}
		                            {!! Form::email('email', old('email'), [
		                                'placeholder' => 'Email',
		                                'class' => 'form-control'
		                            ]) !!}
                                </div>
                        	</div>
                        	
                    		<div class="col-md-6">
                    			<div class="form-group">
                        			{!! Form::label('status', 'Trạng thái',[
                                			'for' => 'status'
                                		]) !!}
					                {!! Form::select('status', ['1' => 'Kích hoạt', '0' => 'Không kích hoạt'], request('status'), [
					                    'class' => 'form-control',
					                ]) !!}
					            </div>
					            
					            <div class="form-group">
                                	{!! Form::label('password', 'Mật khẩu',[
                                			'for' => 'password'
                                		]) !!}
		                            {!! Form::password('password',  [
		                                'placeholder' => 'Mật khẩu',
		                                'class' => 'form-control'
		                            ]) !!}
                                </div>
				            </div>
				            <div class="col-md-6">
				            	<div class="row text-center">
			                        <div class="form-group">
			                            {!! Form::submit('Lưu', [
			                                'class' => 'btn btn-primary btn-fill',
			                            ]) !!}
			                        </div>
		                    	</div>
                        	</div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection