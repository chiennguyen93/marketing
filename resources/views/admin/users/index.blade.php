@extends('layouts.admin')

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Danh sách Người dùng</h4>
                    </div>
                    @include('partials.admin.errors')
                    @include('flash::message')
                    <div class="fixed-table-toolbar">
                        <div class="columns columns-right pull-right">
                            <a href="{{ route('admin.users.create') }}" class="btn btn-default" type="button" name="refresh" title="Refresh"><i class="glyphicon fa fa-plus"></i>Thêm mới</a>
                        </div>
                    </div>
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>Trạng thái</th>
                            <th>Tên</th>
                            <th>Email</th>
                            <th>Hành động</th>
                        </thead>
                        <tbody>
                            @php
                                $i = (request('page', 1) - 1) * 20;
                            @endphp
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>{!! $user->present()->status !!}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td class="td-actions" style="">
                                        <a rel="tooltip" title="" class="btn btn-simple btn-warning btn-icon table-action edit" href="{{ route('admin.users.edit', $user->id) }}" data-original-title="Chỉnh sửa"><i class="fa fa-edit"></i></a>

                                        <a rel="tooltip" title="" class="btn btn-simple btn-danger btn-icon table-action remove" href="{{ route('admin.users.destroy', $user->id) }}" data-original-title="Xóa" data-swal="{{ json_encode(swal([
                                                    'title' => 'Are you sure',
                                                    'type' => 'question',
                                                    'showCancelButton' => true,
                                                ])) }}"
                                                data-swal-method="DELETE"><i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="fixed-table-pagination">
                        <div class="pull-right pagination">
                            {{ $users->appends(request()->query())->links() }}
                        </div>
                    </div>
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

@endsection