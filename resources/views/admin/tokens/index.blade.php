@extends('layouts.admin')

@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">Danh sách Token</h4>
                    </div>
                    <div class="content">
                        @include('partials.admin.errors')
                        @include('flash::message')
                    </div>
                    <div class="fixed-table-toolbar">
                        <div class="columns columns-right pull-right">
                            <a href="{{ route('admin.tokens.create') }}" class="btn btn-default" type="button" name="refresh" title="Refresh"><i class="glyphicon fa fa-plus"></i>Thêm mới</a>
                            <a href="{{ route('admin.tokens.showAddMany') }}" class="btn btn-default" type="button" name="refresh" title="Refresh"><i class="glyphicon fa fa-plus"></i>Thêm nhiều </a>
                        </div>
                    </div>
                    <div class="bars pull-right">
                            <div class="toolbar ">
                                <form class="form-inline" method="GET" action="{{ route('admin.tokens.index') }}">
                                    <input class="form-control" type="text" name="content" placeholder="Token content ..." value="{{ request('content') }}">
                                    <button type="submit" class="btn btn-info">Tìm</button>
                                </form>
                            </div>
                        </div>
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>Trạng thái</th>
                            <th>Content</th>
                            <th>Hành động</th>
                        </thead>
                        <tbody>
                            @php
                                $i = (request('page', 1) - 1) * 20;
                            @endphp
                            @foreach ($tokens as $token)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td>
                                        @if ($token->status == 1)
                                            <span class="label label-success">Acvite</span>
                                        @else
                                            <span class="label label-danger">InAcvite</span>
                                        @endif
                                    </td>
                                    <td>{{ str_limit($token->content,50) }}</td>
                                    <td class="td-actions" style="">
                                        <a rel="tooltip" title="" class="btn btn-simple btn-warning btn-icon table-action edit" href="{{ route('admin.tokens.edit', $token->id) }}" data-original-title="Chỉnh sửa"><i class="fa fa-edit"></i></a>

                                        <a rel="tooltip" title="" class="btn btn-simple btn-danger btn-icon table-action remove" href="{{ route('admin.tokens.destroy', $token->id) }}" data-original-title="Xóa" data-swal="{{ json_encode(swal([
                                                    'title' => 'Are you sure',
                                                    'type' => 'question',
                                                    'showCancelButton' => true,
                                                ])) }}"
                                                data-swal-method="DELETE"><i class="fa fa-remove"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="fixed-table-pagination">
                        <div class="pull-right pagination">
                            {{ $tokens->appends(request()->query())->links() }}
                        </div>
                    </div>
                </div>
                <!--  end card  -->
            </div>
            <!-- end col-md-12 -->
        </div>
        <!-- end row -->
    </div>
</div>

@endsection