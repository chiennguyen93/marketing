@extends('layouts.admin')


@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card" >
                    <div class="header">
                        <legend>Thêm mới Nhiều Token</legend>
                    </div>
                    <div class="content">
                        @include('partials.admin.errors')
                		@include('flash::message')
                    </div>
                    <div class="content">
                    	{!! Form::open() !!}
		                <div class="row">
                        	<div class="col-md-12">
                                <div class="form-group">
                                	{!! Form::label('content', 'Content',[
                                			'for' => 'content'
                                		]) !!}
		                            {!! Form::textarea('content',null , [
                                            'class' => 'form-control',
                                            'rows' => '8',
                                            'placeholder' => 'Các token cách nhau bởi dấy phẩy',
                                        ]) 
                                    !!}
                                </div>
                        	</div>
                        	
                    		<div class="col-md-6">
                    			<div class="form-group">
                        			{!! Form::label('status', 'Trạng thái',[
                                			'for' => 'status'
                                		]) !!}
					                {!! Form::select('status', ['1' => 'Kích hoạt', '0' => 'Không kích hoạt'], request('status'), [
					                    'class' => 'form-control',
					                ]) !!}
					            </div>
				            </div>
				            <div class="col-md-6">
				            	<div class="row text-center">
			                        <div class="form-group">
			                            {!! Form::submit('Lưu', [
			                                'class' => 'btn btn-primary btn-fill',
			                            ]) !!}
			                        </div>
		                    	</div>
                        	</div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection