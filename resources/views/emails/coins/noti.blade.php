@component('mail::message')
Coin: {{ $coin->name }} <br>
Block height: {{ $block->height }} <br>
Transactions: {{ $block->transactions }} <br>
Value: {{ $block->value }} <br>
Time: {{ $block->time->format('H:i:s  d/m/Y') }}

@endcomponent
