require('./bootstrap')
require('./admin/jquery')
require('./admin/jquery-ui')
require('./admin/bootstrap-checkbox-radio-switch-tags')

$(function() {
	require('./shared')
	require('./admin/light-bootstrap-dashboard')


	console.log('here');
	tinymce.init({
	    selector: '#editblog',
	    menubar: false,
	    plugins: [
	      'advlist autolink lists link image charmap print preview anchor',
	      'searchreplace visualblocks code fullscreen',
	      "insertdatetime media nonbreaking save table contextmenu directionality",
	      "textcolor"
	    ],
	    paste_data_images: true,
	    images_upload_url: '/admin/posts/gallery?_token=' + $('input:hidden[name=_token]').val(),
	    image_advtab: true,
	    file_picker_types: 'image',
	    relative_urls: false,
	    convert_urls: false,
	    remove_script_host : false,
	    file_picker_callback: function(callback, value, meta) {
	      if (meta.filetype == 'image') {
	        $('#upload').trigger('click');
	        $('#upload').on('change', function() {
	          var file = this.files[0];
	          var reader = new FileReader();
	          reader.onload = function(e) {
	            callback(e.target.result, {
	              alt: ''
	            });
	          };
	          reader.readAsDataURL(file);
	        });
	      }
	    },
	    toolbar: 'code | undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image |fontselect fontsizeselect | fontselect | forecolor backcolor',
	    fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt 40pt',
	    //font_formats: 'Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n;Roboto=roboto',
	    content_css: [
	      '//www.tinymce.com/css/codepen.min.css',
	      '//fonts.googleapis.com/css?family=Roboto:100,300,400,500,700&subset=vietnamese,latin-ext'
	      ]
    })
})
